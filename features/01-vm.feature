Feature: Manage VMs

  Make sure you've set up ~/.ssh/config as described in the `test.rst` docs.

  To be able to see output from the commands as they are running, use
  `--no-capture`

  Scenario: Ensure valid SSH config 
    Given a valid SSH config for "build1"

  Scenario: Cannot run ping command on non-existent VM, due to no SSH config
    When I run the following admin command:
        """
        build build1-non-existent ping
        """
    Then a "NoValidSSHUserForHost" error is raised

  Scenario: Asking for running status on a VM that doesn't exist is an error
    When I run the following admin command:
        """
        vm local running --img-dir $TEST_DIR build1
        """
    Then a "NoSuchVM" error is raised
 
  Scenario: Create and start a new VM, and issue a successful ping
    Given I run the following admin command successfully:
        """
        vm local create --img-dir $TEST_DIR --private-ip 192.168.33.11 build1
        """
    When I run the following admin command:
        """
        build build1 ping
        """
    Then the admin output is:
        """
        Pong
        """

  Scenario: Successfully report running status
    When I run the following admin command:
        """
        vm local running --img-dir $TEST_DIR build1
        """
    Then the admin output is:
        """
        True
        """

  Scenario: Can't create an existing, running VM
    When I run the following admin command:
        """
        vm local create --img-dir $TEST_DIR --private-ip 192.168.33.21 build1
        """
    Then a "VMAlreadyExists" error is raised


  Scenario: Stop a VM
    When I run the following admin command successfully:
        """
        vm local stop --img-dir $TEST_DIR build1
        """

  Scenario: Successfully report stopped status
    When I run the following admin command:
        """
        vm local running --img-dir $TEST_DIR build1
        """
    Then the admin output is:
        """
        False
        """

  Scenario: Cannot run ping command on stopped VM
    When I run the following admin command:
        """
        build build1 ping
        """
    Then a "NoValidConnectionsError" error is raised

  Scenario: Can't create an existing, stopped VM
     When I run the following admin command:
        """
        vm local create --img-dir $TEST_DIR --private-ip 192.168.33.21 build1
        """
     Then a "VMAlreadyExists" error is raised

  Scenario: Start a VM and issue a ping
    Given I run the following admin command successfully:
        """
        vm local start --img-dir $TEST_DIR build1
        """
    When I run the following admin command:
        """
        build build1 ping
        """
    Then the admin output is:
        """
        Pong
        """

  Scenario: Successfully report running status
    When I run the following admin command:
        """
        vm local running --img-dir $TEST_DIR build1
        """
    Then the admin output is:
        """
        True
        """

  Scenario: Start an already running VM is a no-op, but does not fail
    Given I run the following admin command successfully:
        """
        vm local start --img-dir $TEST_DIR build1
        """

  Scenario: Can still run ping command on the VM after the second started operation
    When I run the following admin command:
        """
        build build1 ping
        """
    Then the admin output is:
        """
        Pong
        """

  Scenario: Still successfully report running status
    When I run the following admin command:
        """
        vm local running --img-dir $TEST_DIR build1
        """
    Then the admin output is:
        """
        True
        """

  Scenario: Cannot destroy a running VM
    When I run the following admin command:
        """
        vm local destroy --img-dir $TEST_DIR build1
        """
    Then a "CannotDestroyRunningVM" error is raised

  Scenario: Stop and destroy the VM
    Given I run the following admin command successfully:
        """
        vm local stop --img-dir $TEST_DIR build1
        """
    And I run the following admin command successfully:
        """
        vm local destroy --img-dir $TEST_DIR build1
        """

  Scenario: Asking for status on a destroyed VM is an error
    When I run the following admin command:
        """
        vm local running --img-dir $TEST_DIR build1
        """
    Then a "NoSuchVM" error is raised

  Scenario: Cannot run ping command on destroyed VM
    When I run the following admin command:
        """
        build build1 ping
        """
    Then a "NoValidConnectionsError" error is raised

  Scenario: Can create and start a new VM with the same name and IP as the old
    Given I run the following admin command successfully:
        """
        vm local create --img-dir $TEST_DIR --private-ip 192.168.33.11 build1
        """

  Scenario: Stop and destroy the VM
    Given I run the following admin command successfully:
        """
        vm local stop --img-dir $TEST_DIR build1
        """
    And I run the following admin command successfully:
        """
        vm local destroy --img-dir $TEST_DIR build1
        """
