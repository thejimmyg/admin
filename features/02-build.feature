Feature: Build dependencies
  Scenario: Create build machine
    Given I run the following admin command successfully:
        """
        vm local create --img-dir $TEST_DIR --private-ip 192.168.33.12 build2
        """

  Scenario: Create a new user:
    Given I run the following admin command successfully:
        """
        build build2 useradd --home-directory $TEST_REMOTE_DEST_USER_DIR $TEST_REMOTE_DEST_USER
        """

  Scenario: Install Python 3
    Given I run the following admin command successfully:
        """
        build build2 python3 --install-directory $TEST_REMOTE_DEST_DIR $TEST_REMOTE_DEST_USER
        """
    And I run the following admin command successfully:
        """
        build build2 pipdeps --install-directory $TEST_REMOTE_DEST_DIR $TEST_REMOTE_DEST_USER
        """

  Scenario: Install monit
    Given I run the following admin command successfully:
        """
        build build2 monit --install-directory $TEST_REMOTE_DEST_DIR $TEST_REMOTE_DEST_USER
        """

  Scenario: Install varnish
    Given I run the following admin command successfully:
        """
        build build2 varnish --install-directory $TEST_REMOTE_DEST_DIR $TEST_REMOTE_DEST_USER
        """

  Scenario: Install postgresql
    Given I run the following admin command successfully:
        """
        build build2 postgresql --install-directory $TEST_REMOTE_DEST_DIR $TEST_REMOTE_DEST_USER
        """

  Scenario: Install redis
    Given I run the following admin command successfully:
        """
        build build2 redis --install-directory $TEST_REMOTE_DEST_DIR $TEST_REMOTE_DEST_USER
        """

  Scenario: Create the build tarball
    Given I run the following admin command successfully:
        """
        build build2 tarball --install-directory $TEST_REMOTE_DEST_DIR $TEST_REMOTE_DEST_USER
        """

  Scenario: Shutdown the VM
    Given I run the following admin command successfully:
        """
        vm local stop --img-dir $TEST_DIR build2
        """
