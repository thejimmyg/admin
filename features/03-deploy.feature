Feature: Install the build
  In a real setup you will want to setup a crontab to start monit and keep it running (replace install_dir)

  # write out current crontab
  crontab -l > mycron.tmp
  # echo new cron into cron file
  echo >> mycron.tmp
  echo "* * * * * {install_directory}/bin/monit -c {install_directory}/etc/monit/monitrc" >> mycron.tmp
  echo "* * * * * {install_directory}/bin/monit -c {install_directory}/etc/monit/monitrc" >> mycron.tmp
  # install new cron file
  crontab mycron.tmp
  rm mycron.tmp

  Scenario: Install the build tarball on a new VM
    Given a valid SSH config for "build3"
    And I run the following admin command successfully:
        """
        vm local create --img-dir $TEST_DIR --private-ip 192.168.33.13 build3
        """
    And I run the following admin command:
        """
        build build3 ping
        """
    And the admin output is:
        """
        Pong
        """
    And I run the following admin command successfully:
        """
        build build3 useradd --home-directory $TEST_REMOTE_DEST_USER_DIR $TEST_REMOTE_DEST_USER
        """
    And I run the following admin command successfully:
        """
        build build3 install --install-directory $TEST_REMOTE_DEST_DIR --username $TEST_REMOTE_DEST_USER build.tar.gz
        """ 
    When I run the following admin command:
        """
        build build3 testpython --install-directory $TEST_REMOTE_DEST_DIR --username $TEST_REMOTE_DEST_USER
        """ 
    Then the admin output is:
        """
        1.9.8
        """

  Scenario: Setup monit config
    Given I run the following admin command successfully:
        """
        build build3 monit_etc --install-directory $TEST_REMOTE_DEST_DIR $TEST_REMOTE_DEST_USER
        """
