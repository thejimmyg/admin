import difflib
import sys
import traceback

from admin import cmd, load_ssh_config
from behave import *


@step('a valid SSH config for "{host}"')
def step_impl(context, host):
    # Will raise if not valid
    return load_ssh_config(host)


@step('I run the following admin command')
def step_impl(context):
    try:
        output = cmd(context.text, out=None)
    except Exception as e:
        import traceback
        traceback.print_exc()
        context.admin_command = (False, sys.exc_info())
    else:
        context.admin_command = (True, output)

@step('I run the following admin command successfully')
def step_impl(context):
    cmd(context.text, out=print) 

# @step('I run the following admin command successfully')
# def step_impl(context):
#     context.execute_steps('''\
#         Given I run the following admin command
#           And the admin command is successful
#     ''')
    
@step('the admin command is successful')
def step_impl(context):
    assert context.admin_command is not None, "No admin command has been run yet, so there is no error"
    if context.admin_command[0] is False:
        exc_info = context.admin_command[1]
        raise exc_info[1].with_traceback(exc_info[2])
    context.admin_command = None

@step('a "{error}" error is raised')
def step_impl(context, error):
    assert context.admin_command is not None, "No admin command has been run yet, so there is no error"
    assert context.admin_command[0] is False, "The admin command succeeded"
    exc_info = context.admin_command[1]
    if exc_info[1].__class__.__name__ != error:
        raise exc_info[1].with_traceback(exc_info[2])
    context.admin_command = None

@step('there is no admin output')
def step_impl(context):
    assert context.admin_command is not None, "No admin command has been run yet, so there is no error"
    exc_info = context.admin_command[1]
    if context.admin_command[0] is False:
        raise exc_info[1].with_traceback(exc_info[2])
    assert context.text == None, 'Unexpected output:\n{!s}'.format(context.text)
    assert context.admin_command[1] == [], 'Unexpected output:\n{!s}'.format(context.admin_command[1])
    context.admin_command = None

@step('the admin output is')
def step_impl(context):
    assert context.admin_command is not None, "No admin command has been run yet, so there is no error"
    exc_info = context.admin_command[1]
    if context.admin_command[0] is False:
        raise exc_info[1].with_traceback(exc_info[2])
    if context.admin_command[1].rstrip('\n') != context.text.rstrip('\n'):
        s1 = [s+'\n' for s in context.text.split('\n')]
        s2 = [s+'\n' for s in context.admin_command[1].split('\n')]
        assert False, '\n'+(''.join(difflib.unified_diff(s1, s2, fromfile='expected', tofile='actual')))
    context.admin_command = None
