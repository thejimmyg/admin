import os
import time

from behave import *


@step('I navigate to /{path}')
def step_impl(context, path):
    context.browser.get(context.config.userdata['test_host'] + '/' + path)

@step('the browser moves to /{path}')
def step_impl(context, path):
    expected_url = context.config.userdata['test_host'] + '/' + path
    assert expected_url == context.browser.current_url, "Expected browser to be at {!r} but it as at {!r}".format(expected_url, context.browser.current_url)

@step('I click on "{selector}"')
def step_impl(context, selector):
    element = context.browser.find_element_by_css_selector(selector)
    assert element is not None, "No such element found"
    element.click()

@step('I follow the "{text}" link')
def step_impl(context, text):
    element = context.browser.find_element_by_link_text(text)
    assert element is not None, "No such link found"
    element.click()

@step('I wait for {seconds:f} seconds')
def step_impl(context, seconds):
    time.sleep(seconds)

@step('I wait for {seconds:d} seconds')
def step_impl(context, seconds):
    time.sleep(seconds)

@step('I wait for 1 second')
def step_impl(context):
    time.sleep(1)

@step('there are {n} "{selector}" elements within "{container_selector}"')
def step_impl(context, n, selector, container_selector):
    container = context.browser.find_element_by_css_selector(container_selector)
    elements = container.find_elements_by_css_selector(selector)
    assert len(elements) != n, "Expected {} elements, got {}".format(n, len(elements))

@step('there is 1 "{selector}" element within "{container_selector}"')
def step_impl(context, selector, container_selector):
    context.execute_steps('Then there are 1 "%s" elements within "%s"' % (selector, container_selector))

@step('there are {n:d} "{selector}" elements')
def step_impl(context, n, selector):
    context.execute_steps('Then there are %d "%s" elements within "html"' % (n, selector))

@step('there is 1 "{selector}" element')
def step_impl(context, selector):
    context.execute_steps('Then there are 1 "%s" elements within "html"' % (selector))

@step('I see "{text}" within "{container}"')
def step_impl(context, text, container):
    element = context.browser.find_element_by_css_selector(container)
    assert element is not None and text in element.get_attribute('textContent'), "Did not find text"

import subprocess

@step('I run the following command')
def step_impl(context):
    context.output = None
    context.p = subprocess.Popen(
        context.text,
        cwd=context.config.userdata.get("cwd", os.getcwd()),
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )


@step('I run the following command successfully')
def step_impl(context):
    context.execute_steps('''
        Given I run the following command
          """
          {}
          """
    '''.format(context.text))
    out, _ = context.p.communicate()
    exit_code = context.p.wait()
    assert int(exit_code) == 0, "Unexpected exit code {}\n{}".format(exit_code, out)

import difflib

@step('there is no output')
def step_impl(context):
    out, _ = context.p.communicate()
    exit_code = context.p.wait()
    assert 0 == exit_code, "Unexpected exit code {}".format(exit_code)
    assert out == b'' or out is None, 'Unexpected output:\n{!s}'.format(out)

@step('the output is')
def step_impl(context):
    return output_is(context.p, 0, context.text)

@step('the output contains')
def step_impl(context):
    return output_contains(context.p, 0, context.text)

@step('the exit code is "{code:d}" and the output is')
def step_impl(context, code):
    return output_is(context.p, 0, context.text)

def output_is(p, code, text):
    out, _ = p.communicate()
    exit_code = p.wait()
    if out != text.encode('utf8'):
        s1 = [s+'\n' for s in text.split('\n')]
        s2 = [s+'\n' for s in str(out, 'utf8').split('\n')]
        diff = '\n'+(''.join(difflib.unified_diff(s1, s2, fromfile='expected', tofile='actual')))
        assert int(code) == exit_code, "Unexpected exit code {}\n{}".format(exit_code, diff)
        assert False, diff
    else:
        assert int(code) == exit_code, "Unexpected exit code {}".format(exit_code)

@step('the exit code is "{code:d}" and the output contains')
def step_impl(context, code):
    return output_contains(context.p, code, context.text)

def output_contains(p, code, text):
    out, _ = p.communicate()
    exit_code = p.wait()
    assert int(code) == exit_code, "Unexpected exit code {}\n{}".format(exit_code, out)
    assert text.strip().encode('utf8') in out, "Expected text not found in: {!r}".format(text)

@step('I type "{text}" into "{selector}"')
def step_impl(context, text, selector):
    element = context.browser.find_element_by_css_selector(selector)
    assert element is not None, "No such element found"
    element.send_keys(text)

@step('I type into "{selector}"')
def step_impl(context, selector):
    element = context.browser.find_element_by_css_selector(selector)
    assert element is not None, "No such element found"
    element.send_keys(context.text)
