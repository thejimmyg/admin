Feature: Deploy a WSGI app, start and stop it

  Scenario: Create the demo app
    Given I run the following command successfully:
      """
      rm -f ../demo.tar.gz
      """
      And I run the following command successfully:
      """
      cd ../ && tar -czvf demo.tar.gz demo
      """

  Scenario: Deploy a WSGI app
    Given a valid SSH config for "build3"
    And I run the following admin command successfully:
        """
        build build3 monit_start --install-directory $TEST_REMOTE_DEST_DIR --username $TEST_REMOTE_DEST_USER
        """ 
    And I run the following admin command successfully:
        """
        wsgi build3 app --install-directory $TEST_REMOTE_DEST_DIR --username $TEST_REMOTE_DEST_USER --name demo demo.tar.gz 
        """
    And I run the following admin command successfully:
        """
        wsgi build3 monit --install-directory $TEST_REMOTE_DEST_DIR --username $TEST_REMOTE_DEST_USER --name demo --port 8080 --host 192.168.33.13
        """
    And I wait for 2 seconds

  Scenario: Start the app
    Given I run the following admin command successfully:
        """
        build build3 start --install-directory $TEST_REMOTE_DEST_DIR --username $TEST_REMOTE_DEST_USER demo
        """
    And I wait for 2 seconds
    When I run the following command:
        """
        curl –silent http://192.168.33.13:8080
        """
    Then the output contains:
        """
        wsgi.multiprocess
        """

  Scenario: Stop the app
    Given I run the following admin command successfully:
        """
        build build3 stop --install-directory $TEST_REMOTE_DEST_DIR --username $TEST_REMOTE_DEST_USER demo
        """
      And I wait for 2 seconds
     When I run the following command:
        """
        curl –silent http://192.168.33.13:8080
        """
     Then the exit code is "56" and the output contains:
        """
        Recv failure
        """
      And I run the following admin command successfully:
        """
        vm local stop --img-dir $TEST_DIR build3
        """
