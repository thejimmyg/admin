import os


def before_all(context):
    for var in ['TEST_REMOTE_DEST_DIR', 'TEST_DIR', 'TEST_REMOTE_DEST_USER', 'TEST_REMOTE_DEST_USER_DIR']:
        assert var not in os.environ, 'Expected the {} envrionment variable not be set'.format(var)
    if not os.path.exists(context.config.userdata['cwd']):
        os.mkdir(context.config.userdata['cwd'])
    os.environ['TEST_DIR'] = context.config.userdata['cwd']
    os.environ['TEST_REMOTE_DEST_DIR'] = context.config.userdata['remote_dest_dir']
    os.environ['TEST_REMOTE_DEST_USER'] = context.config.userdata['remote_dest_user']
    os.environ['TEST_REMOTE_DEST_USER_DIR'] = context.config.userdata['remote_dest_user_dir']


def after_all(context):
    for var in ['TEST_REMOTE_DEST_DIR', 'TEST_DIR', 'TEST_REMOTE_DEST_USER', 'TEST_REMOTE_DEST_USER_DIR']:
        del os.environ[var]
