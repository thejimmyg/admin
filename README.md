# Admin

See `docs/index.rst` to get started.

After install you can also build docs or run the tests:

Docs:

```
pip install -r requirements/dev.txt
cd docs
make html
```

Get HTTPS vcertificates:

```
sudo certbot certonly --manual
sudo cat /etc/letsencrypt/live/jimmyg.org/privkey.pem > key.pem
sudo cat /etc/letsencrypt/live/jimmyg.org/cert.pem > cert.pem
sudo cat /etc/letsencrypt/live/jimmyg.org/fullchanin.pem > fullchain.pem
openssl dhparam -rand - 2048 > dh.pem
cat key.pem fullchain.pem dh.pem > jimmyg.org.pem
cat key.pem cert.pem dh.pem > jimmyg.org.pem
cat fullchain.pem dh.pem > jimmyg.org.pem
curl https://letsencrypt.org/certs/lets-encrypt-x3-cross-signed.pem > lets-encrypt-x3-cross-signed.pem
cat key.pem cert.pem fullchain.pem lets-encrypt-x3-cross-signed.pem dh.pem > jimmyg.org.pem
scp jimmyg.org.pem dorun:platform/etc/ssl/jimmyg.org.pem
python -m admin.monit dorun stop --install-directory /root/platform --username root demo_ssl
python -m admin.monit dorun start --install-directory /root/platform --username root demo_ssl
```

Make sure the domain name points to the correct place.


When you make changes to a consumer, you need to restart the worker:

```
python -m admin.monit dorun stop  --install-directory /opt/jimmyg/platform --username jimmyg django_worker
python -m admin.monit dorun start --install-directory /opt/jimmyg/platform --username jimmyg django_worker
```

When you change the TLS settings:

```
python -m admin.deploy dorun reload_monit_config --install-directory /root/platform --username root
python -m admin.monit dorun stop --install-directory /root/platform --username root demo_ssl
python -m admin.monit dorun start --install-directory /root/platform --username root demo_ssl
```

python -m admin.monit dorun stop --install-directory /root/platform --username root django_daphne
python -m admin.monit dorun start --install-directory /root/platform --username root django_daphne
