#!/usr/bin/env python

from setuptools import setup

setup(
    name='admin',
    version='0.1.0',
    description='Build and deploy services',
    author='James Gardner',
    author_email='james@pythonweb.org',
    url='https://jimmyg.org',
    package_dir={'admin':'admin'},
)
