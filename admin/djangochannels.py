'''
socket = new WebSocket("wss://" + window.location.host + "/chat/");
socket.onmessage = function(e) {
    alert(e.data);
}
socket.onopen = function() {
    socket.send("hello world");
}
'''


import os
from . import sudo


daphne_app_template = '''\
check process {name}_daphne with pidfile {install_directory}/pid/{name}_daphne.pid
   start program = "{install_directory}/etc/monit/init.d/daphne.sh start"
   stop program =  "{install_directory}/etc/monit/init.d/daphne.sh stop"
   DEPENDS on {name}_worker
   # if failed port {port} protocol http with timeout 3 seconds then restart
   # if failed host {host} port {port} protocol http request "/" with HOSTHEADER "{hostheader}" then restart
   # if 2 restarts within 5 cycles then alert
'''

worker_app_template = '''\
check process {name}_worker with pidfile {install_directory}/pid/{name}_worker.pid
   start program = "{install_directory}/etc/monit/init.d/worker.sh start"
   stop program =  "{install_directory}/etc/monit/init.d/worker.sh stop"
   DEPENDS on redis
   # if failed port {port} protocol http with timeout 3 seconds then restart
   if failed host {host} port {port} protocol http request "/" with HOSTHEADER "{hostheader}" then restart
   # if 2 restarts within 5 cycles then alert
'''




daphne_template = '''\
#!/bin/sh

. /opt/{name}/app/env.sh

DIR="/opt/{name}"
DAEMON="$DIR/platform/bin/daphne"
DAEMON_NAME=daphne

export PYTHONPATH=$DIR/app/{name}:$PYTHONPATH
export PATH=/opt/{name}/platform/bin:/opt/{name}/platform/sbin:$PATH
export LD_LIBRARY_PATH=/opt/{name}/platform/lib:$LD_LIBRARY_PATH

# Add any command line options for your daemon here
DAEMON_OPTS="--port {port} --bind {host} {module}.asgi:channel_layer"

# This next line determines what user the script runs as.
DAEMON_USER="{username}"

# The process ID of the script when it runs is stored here:
PIDFILE="$DIR/platform/pid/{name}_$DAEMON_NAME.pid"

do_start () {{
    echo "Starting system $DAEMON_NAME daemon"
    /sbin/start-stop-daemon --start --chdir $DIR/app --background --no-close --retry 1 --pidfile $PIDFILE --make-pidfile --user $DAEMON_USER --chuid $DAEMON_USER:$DAEMON_USER --startas /bin/bash -- -c "exec $DAEMON $DAEMON_OPTS >> $DIR/platform/log/$DAEMON_NAME.log 2>&1"
}}
do_stop () {{
    echo "Stopping system $DAEMON_NAME daemon"
    /sbin/start-stop-daemon --stop --pidfile $PIDFILE --retry 10
}}

case "$1" in

    start|stop)
        do_${{1}}
        ;;

    restart)
        do_stop
        do_start
        ;;

    *)
        echo "Usage: /etc/init.d/$DAEMON_NAME {{start|stop|restart}}"
        exit 1
        ;;

esac
exit 0
'''


worker_template = '''\
#!/bin/sh

. /opt/{name}/app/env.sh

DIR="/opt/{name}"
DAEMON="$DIR/platform/bin/python3.5"
DAEMON_NAME=worker
export LD_LIBRARY_PATH=/opt/{name}/platform/lib:$LD_LIBRARY_PATH

export PYTHONPATH=$DIR/app/{name}:$PYTHONPATH
export PATH=/opt/{name}/platform/bin:/opt/{name}/platform/sbin:$PATH

# Add any command line options for your daemon here
DAEMON_OPTS="/opt/{name}/app/{module}/manage.py runworker"

# This next line determines what user the script runs as.
DAEMON_USER="{username}"

# The process ID of the script when it runs is stored here:
PIDFILE="$DIR/platform/pid/{name}_$DAEMON_NAME.pid"

do_start () {{
    echo "Starting system $DAEMON_NAME daemon"
    /sbin/start-stop-daemon --start --chdir $DIR/app --background --no-close --retry 1 --pidfile $PIDFILE --make-pidfile --user $DAEMON_USER --chuid $DAEMON_USER:$DAEMON_USER --startas /bin/bash -- -c "exec $DAEMON $DAEMON_OPTS >> $DIR/platform/log/$DAEMON_NAME.log 2>&1"
}}
do_stop () {{
    echo "Stopping system $DAEMON_NAME daemon"
    /sbin/start-stop-daemon --stop --pidfile $PIDFILE --retry 10
}}

case "$1" in

    start|stop)
        do_${{1}}
        ;;

    restart)
        do_stop
        do_start
        ;;

    *)
        echo "Usage: /etc/init.d/$DAEMON_NAME {{start|stop|restart}}"
        exit 1
        ;;

esac
exit 0
'''


def monit(run, username, install_directory, name, module=None, port=8080, host='127.0.0.1', hostheader=None, out=None):
    if module is None:
        module = name
    if hostheader is None:
        hostheader = host
    output = ''
    # Daphne
    output += run(
        'cat - | ' +
        sudo(
            user=username,
            command='tee {}/etc/monit/conf.d/{}_daphne.conf'.format(install_directory, name)
        ),
        out,
        daphne_app_template.format(
            install_directory=install_directory,
            name=name,
            port=port,
            host=host,
            hostheader=hostheader,
        ),
    )
    output += run(sudo(username, 'chmod 600 {install_directory}/etc/monit/conf.d/{name}_daphne.conf'.format(install_directory=install_directory, name=name)), out)
    output += run(
        'cat - | ' +
        sudo(
            user=username,
            command='tee {}/etc/monit/init.d/daphne.sh'.format(install_directory, name)
        ),
        out,
        daphne_template.format(
            install_directory=install_directory,
            username=username,
            name=name,
            port=port,
            host=host,
            module=module,
        ),
    )
    output += run(sudo(username, 'chmod +x {install_directory}/etc/monit/init.d/daphne.sh'.format(install_directory=install_directory, name=name)), out)

    # Worker
    output += run(
        'cat - | ' +
        sudo(
            user=username,
            command='tee {}/etc/monit/conf.d/{}_worker.conf'.format(install_directory, name)
        ),
        out,
        worker_app_template.format(
            install_directory=install_directory,
            name=name,
            port=port,
            host=host,
            hostheader=hostheader,
        ),
    )
    output += run(sudo(username, 'chmod 600 {install_directory}/etc/monit/conf.d/{name}_worker.conf'.format(install_directory=install_directory, name=name)), out)
    output += run(
        'cat - | ' +
        sudo(
            user=username,
            command='tee {}/etc/monit/init.d/worker.sh'.format(install_directory, name)
        ),
        out,
        worker_template.format(
            install_directory=install_directory,
            username=username,
            name=name,
            port=port,
            host=host,
            module=module,
        ),
    )
    output += run(sudo(username, 'chmod +x {install_directory}/etc/monit/init.d/worker.sh'.format(install_directory=install_directory, name=name)), out)
    return output


def app(run, local_file, username, directory, ssh_path, out=None):
    output = ''
    output += run(sudo(username, 'mkdir -p {}'.format(directory)), out)
    print(local_file, '{ssh_path}/app.tar.gz'.format(ssh_path=ssh_path))
    run.put(
        localpath = local_file,
        filepath = '{ssh_path}/app.tar.gz'.format(ssh_path=ssh_path),
    )
    output += run(
        sudo(
            cwd=directory,
            command='mv {ssh_path}/app.tar.gz . ; chown {username} app.tar.gz'.format(username=username, ssh_path=ssh_path),
       ),
       out
    )
    output += run(
        sudo(
            user=username,
            cwd='{}'.format(directory),
            command='tar -xzf app.tar.gz && rm app.tar.gz'
       ),
       out
    )
    return output



actions = {
    'monit': monit,
    'app': app,
}


if __name__ == '__main__':
    import sys
    from . import run_command

    run_command(actions, sys.argv[1:])
