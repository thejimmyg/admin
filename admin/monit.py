import os
from . import sudo

def stop(run, name, username, install_directory, out=None):
    return run(sudo(username, '{install_directory}/bin/monit -c {install_directory}/etc/monit/monitrc stop {name}'.format(name=name, install_directory=install_directory)), out)


def start(run, name, username, install_directory, out=None):
    return run(sudo(username, '{install_directory}/bin/monit -c {install_directory}/etc/monit/monitrc start {name}'.format(name=name, install_directory=install_directory)), out)


def status(run, name, username, install_directory, out=None):
    return run(sudo(username, '{install_directory}/bin/monit -c {install_directory}/etc/monit/monitrc status {name}'.format(name=name, install_directory=install_directory)), out)


actions = {
    'start': start,
    'stop': stop,
    'status': status,
}


if __name__ == '__main__':
    import sys
    from . import run_command

    run_command(actions, sys.argv[1:])
