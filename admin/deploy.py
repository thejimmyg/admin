import os
from . import sudo


def testpython(run, username, install_directory=None, out=None):
    output = ''
    cmd = sudo(
        user=username,
        cwd='{}'.format(install_directory),
        command='bin/python3 -c "import django; print(django.get_version())"'.format(install_directory),
    )
    output += run(cmd, out)
    return output


def install(run, local_file, username, directory, ssh_path, out=None):
    output = ''
    # output += run(sudo(username, 'mkdir -p {}'.format(install_directory)), out)
    # output += run(sudo(username, 'mkdir -p {}/pid'.format(install_directory)), out)
    output += run(sudo(username, 'mkdir -p {}'.format(directory)), out)
    print(local_file, '{ssh_path}/build-upload.tar.gz'.format(ssh_path=ssh_path))
    run.put(
        localpath = local_file,
        filepath = '{ssh_path}/build-upload.tar.gz'.format(ssh_path=ssh_path),
    )
    output += run(
        sudo(
            cwd=directory,
            command='mv {ssh_path}/build-upload.tar.gz . ; chown {username} build-upload.tar.gz'.format(username=username, ssh_path=ssh_path),
       ),
       out
    )
    output += run(
        sudo(
            user=username,
            cwd='{}'.format(directory),
            command='tar -xzf build-upload.tar.gz && rm build-upload.tar.gz'
       ),
       out
    )
    return output


def setup_monit_config(
    run,
    username,
    install_directory,
    alert_email,
    smtp_server,
    smtp_username,
    smtp_password,
    hostname,
    listen='localhost',
    port=8064,
    password='pass',
    out=None
):
    assert password != 'password', "Monit gives a syntax error with the password 'password'"
    output = ''
    output += run(sudo(username, 'mkdir -p {}/etc/monit/conf.d'.format(install_directory)), out)
    output += run(sudo(username, 'mkdir -p {}/etc/monit/init.d'.format(install_directory)), out)
    output += run(sudo(username, 'mkdir -p {}/log'.format(install_directory)), out)
    output += run(sudo(username, 'mkdir -p {}/pid'.format(install_directory)), out)
    output += run(
        'cat - | ' +
        sudo(
            user=username,
            command='tee {}/etc/monit/monitrc'.format(install_directory)
        ),
        out,
        monitrc_template.format(
            smtp_server=smtp_server,
            smtp_username=smtp_username,
            smtp_password=smtp_password,
            listen=listen,
            hostname=hostname,
            alert_email=alert_email,
            install_directory=install_directory,
            port=port,
            password=password
        ),
    )
    output += run(sudo(username, 'chmod 600 {install_directory}/etc/monit/monitrc'.format(install_directory=install_directory, password=password)), out)

    output += run(sudo(username, '/usr/bin/openssl req -new -x509 -days 3650 -nodes -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=example.com" -out {install_directory}/etc/monit/monit.pem -keyout {install_directory}/etc/monit/monit.pem'.format(install_directory=install_directory)), out)

    output += run('/usr/bin/openssl gendh 1024 | ' + sudo(username, 'tee -a {install_directory}/etc/monit/monit.pem'.format(install_directory=install_directory)), out)
    output += run(sudo(username, 'chmod 600 {install_directory}/etc/monit/monit.pem'.format(install_directory=install_directory)), out)
    # /usr/bin/openssl x509 -text -noout -in etc/monit/monit.pem
    return output


def run_monit(run, username, install_directory, out=None):
    output = []
    output += run(sudo(username, 'mkdir -p {}/tmp'.format(install_directory)), out)
    output += run(sudo(username, '{install_directory}/bin/monit -c {install_directory}/etc/monit/monitrc'.format(install_directory=install_directory)), out)
    output += run(
        'cat - | ' + sudo(username, 'tee {install_directory}/tmp/mycron'.format(install_directory=install_directory)),
        out,
        "* * * * * {install_directory}/bin/monit -c {install_directory}/etc/monit/monitrc\n".format(install_directory=install_directory)
    )
    output += run(sudo(username, 'crontab {install_directory}/tmp/mycron'.format(install_directory=install_directory)), out)
    return output


def quit_monit(run, username, install_directory, out=None):
    return run(sudo(username, '{install_directory}/bin/monit -c {install_directory}/etc/monit/monitrc quit'.format(install_directory=install_directory)), out)


def reload_monit_config(run, username, install_directory, out=None):
    return run(sudo(username, '{install_directory}/bin/monit -c {install_directory}/etc/monit/monitrc reload'.format(install_directory=install_directory)), out)


def stop(run, name, username, install_directory, out=None):
    return run(sudo(username, '{install_directory}/bin/monit -c {install_directory}/etc/monit/monitrc stop {name}'.format(name=name, install_directory=install_directory)), out)


def start(run, name, username, install_directory, out=None):
    return run(sudo(username, '{install_directory}/bin/monit -c {install_directory}/etc/monit/monitrc start {name}'.format(name=name, install_directory=install_directory)), out)


def add_swap(run, out=None):
    output = []
    output += run(sudo(command='fallocate -l 1G /swapfile'))
    output += run(sudo(command='chmod 600 /swapfile'))
    output += run(sudo(command='mkswap /swapfile'))
    output += run(sudo(command='swapon /swapfile'))
    output += run("echo '/swapfile   none    swap    sw    0   0' | " + sudo(command='tee -a /etc/fstab'))
    return output


def savefirewall(run, out=None):
    output = []
    output += run(sudo('root', 'iptables-save | sudo tee /etc/iptables.rules.v4'.format()), out)
    output += run(
        'cat - | ' +
        sudo(
            'root',
            command='tee /etc/network/if-pre-up.d/iptablesload'
        ),
        out,
        '''\
#!/bin/sh
/sbin/iptables-restore < /etc/iptables.rules.v4
exit 0'''
    )
    output += run(sudo('root', 'chmod +x /etc/network/if-pre-up.d/iptablesload'), out)
    return output


def redirectport(run, src, dst, out=None):
    output = []
    output += run(sudo('root', 'iptables -t nat -I PREROUTING -p tcp --dport {src} -j REDIRECT --to-ports {dst}'.format(dst=dst, src=src)), out)
    output += run(sudo('root', 'iptables -t nat -I OUTPUT -p tcp -o lo --dport {src} -j REDIRECT --to-ports {dst}'.format(dst=dst, src=src)), out)
    return output


def clearfirewall(run, out=None):
    output = []
    for line in '''iptables -F
iptables -X
iptables -t nat -F
iptables -t nat -X
iptables -t mangle -F
iptables -t mangle -X
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT'''.split('\n'):
        output += run(sudo('root', line), out)
        print(line)
    return output


def add_ssh_key(run, username, home_directory, key_path, out=None):
    output = ''
    output += run(sudo(username, 'mkdir -p {}/.ssh'.format(home_directory)), out)
    output += run(sudo(username, 'chmod 700 {}/.ssh'.format(home_directory)), out)
    with open(key_path, 'rb') as fp:
        data = fp.read()
    output += run(
        'cat - | ' +
        sudo(
            user=username,
            command='tee -a {}/.ssh/authorized_keys'.format(home_directory)
        ),
        out,
        data
    )
    output += run(sudo(username, 'chmod 600 {}/.ssh/authorized_keys'.format(home_directory)), out)
    return output


monitrc_template = '''\
set daemon 20
set logfile {install_directory}/log/monit.log
set pidfile {install_directory}/pid/monit.pid
set alert {alert_email}

SET MAILSERVER
    {smtp_server}
    PORT 587
    USERNAME {smtp_username} PASSWORD {smtp_password}
    using tls
    using HOSTNAME {hostname}

set mail-format {{
      from: Monit Support <{alert_email}>
}}


# Test with:
#   curl --insecure -u admin:pass  https://localhost:{port}
set httpd port {port} and
    use address {listen}
    allow admin:{password}
    SSL ENABLE
    PEMFILE  {install_directory}/etc/monit/monit.pem

include {install_directory}/etc/monit/conf.d/*
'''


actions = {
    'clearfirewall': clearfirewall,
    'add_swap': add_swap,
    'add_ssh_key': add_ssh_key,
    'redirectport': redirectport,
    'savefirewall': savefirewall,
    'install': install,
    'testpython': testpython,
    # monit daemon
    'setup_monit_config': setup_monit_config,
    'run_monit': run_monit, # Doesn't start the services
    'quit_monit': quit_monit,
    'reload_monit_config': reload_monit_config,
}


if __name__ == '__main__':
    import sys
    from . import run_command

    run_command(actions, sys.argv[1:])
