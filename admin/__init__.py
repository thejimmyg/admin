import os
import subprocess
import unittest
import shlex
import importlib

import paramiko


def hide(line):
    pass


class Runner:
    def __call__(self, command, out=None, input=None):
        return self.run(command, out, input)


def load_ssh_config(host):
    ssh_config = paramiko.SSHConfig()
    user_config_file = os.path.expanduser("~/.ssh/config")
    if os.path.exists(user_config_file):
        with open(user_config_file) as fp:
            ssh_config.parse(fp)
    user_config = ssh_config.lookup(host)
    if not 'user' in user_config:
        raise NoValidSSHUserForHost(user_config)
    config = {
        'hostname': user_config['hostname'],
        'username': user_config['user'],
        'port': int(user_config.get('port', '22')),
        'key_filename': user_config.get('identityfile'),
    }
    return config


class SSHRunner(Runner):
    def __init__(self, host):
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh.connect(**load_ssh_config(host))

    def run(self, command, out=None, input=None):
        tran = self.ssh.get_transport()
        chan = tran.open_session()
        chan.set_combine_stderr(True)
        chan.exec_command(command)
        stdin = chan.makefile('wb', -1)
        stdout = chan.makefile('rb', -1)
        if input:
            stdin.write(input)
            stdin.flush()
            stdin.channel.shutdown_write()
        output = []
        last_line = None
        while True:
            line = stdout.readline()
            if line != b'':
                decoded_line = str(line, 'utf8')
                last_line = decoded_line.rstrip()
                if out:
                    out(last_line)
                output.append(decoded_line)
            else:
            	break
        status = stdout.channel.recv_exit_status()
        if status != 0:
            raise Exception(command, last_line, status)
        return ''.join(output)

    def get(self, file_path, local_path):
        transport = self.ssh.get_transport()
        sftp = paramiko.SFTPClient.from_transport(transport)
        # sftp.get_channel()
        # chan.exec_command('sudo su -c /bin/sftp-server')
        sftp.get(file_path, local_path)
        # filepath = '/home/foo.jpg'
        # localpath = '/home/pony.jpg'
        # sftp.put(localpath, filepath)
        # Close
        sftp.close()
        # transport.close()

    def put(self, localpath, filepath):
        transport = self.ssh.get_transport()
        sftp = paramiko.SFTPClient.from_transport(transport)
        sftp.put(localpath, filepath)
        sftp.close()


class LocalRunner(Runner):
    def run(self, command, out=None, input=None):
        stdin = None
        if input != None:
            stdin = subprocess.PIPE
        process = subprocess.Popen(
            command,
            shell=True,
            stderr=subprocess.STDOUT,
            stdout=subprocess.PIPE,
            stdin=stdin,
        )
        if input:
            process.stdin.write(input)
            process.stdin.flush()
            process.stdin.close()
        output = []
        last_line = None
        while True:
            line = process.stdout.readline()
            if line != b'':
                decoded_line = str(line, 'utf8')
                last_line = decoded_line.rstrip()
                if out:
                    out(last_line)
                output.append(decoded_line)
            else:
                process.stdout.close()
                break
        status = process.wait()
        if status != 0:
            raise Exception(command, last_line, status)
        return ''.join(output)


class MultiRunner(Runner):
    def __init__(self, runners):
        self.runners = runners

    def run(self, command, out=None, input=None):
        res = []
        for run in self.runners:
            res.append(run(command, out, input))
        return res

# XXX TODO: Don't create a new VM if there is already an SSH Config for the host
# class TestSSHConfig(unittest.TestCase):
#     def test_host_exists(self):
#         pass


class TestRunner(unittest.TestCase):
    def test_local(self):
        run = LocalRunner()
        self.assertEqual(run("echo 'Hello'"), 'Hello\n')
        l = []
        def out(line):
            l.append(line)
        run("echo 'Hello'", out)
        self.assertEqual(len(l), 1)
        self.assertEqual(l[0], 'Hello')

    def test_ssh(self):
        run = SSHRunner('build2')
        self.assertEqual(run("echo 'Hello'"), 'Hello\n')
        l = []
        def out(line):
            l.append(line)
        run("echo 'Hello'", out)
        self.assertEqual(len(l), 1)
        self.assertEqual(l[0], 'Hello')

    def test_multi(self):
        local = LocalRunner()
        ssh = SSHRunner('build2')
        run = MultiRunner([local, ssh])
        self.assertEqual(run("echo 'Hello'"), ['Hello\n', 'Hello\n'])


def cmd(s, out=None):
    args = [os.path.expandvars(arg) for arg in shlex.split(s)]
    module = importlib.import_module('.'+args[0], 'admin')
    actions = module.actions
    return run_command(actions, args[1:], out=out)


class NoValidSSHUserForHost(Exception):
    pass

def run_command(actions, args, out=print):
    assert len(args) >= 2, 'Not enough arguments'
    assert not args[0].startswith('_'), 'Hostnames cannot start with _'
    assert not args[1].startswith('_'), 'Cannot call private functions'
    assert args[1] in actions, 'No such action {!r}'.format(args[1])
    named = {}
    hosts = args[0].split(',')
    runners = []
    found_local = False
    for host in hosts:
        if host == 'local':
            assert not found_local, 'Have already added a local runner'
            found_local = True
            runners.append(LocalRunner())
        else:
            runners.append(SSHRunner(host))
    args = args[1:]
    i = 0
    while i < len(args)-1:
        arg = args[1+i]
        if arg.startswith('--'):
            opt = arg[2:].replace('-', '_')
            assert '=' not in opt, 'Did not expect = in the option name for {}'.format(arg)
            assert 1+i+1 < len(args), 'No value specified for {}'.format(arg)
            value = args[1+i+1]
            assert opt not in named, 'Cannot have duplicate options {!r}'.format(arg)
            named[arg[2:].replace('-', '_')] = value
            i += 2
        else:
            break
    pos = args[1+i:]
    # print(actions, args[0], runners, pos, named)
    if len(runners) > 1:
        run = MultiRunner(runners)
    else:
        run = runners[0]
    assert 'out' not in named, 'Invalid option --out'
    named['out'] = out
    return actions[args[0]](run, *pos, **named)


class TestRunCommand(unittest.TestCase):
    def test_simple(self):

        def hello(run, one, two='two_default', out=None):
            self.assertTrue(hasattr(run, 'run'))
            return [one, two]

        self.assertEqual(
            run_command({'hello': hello}, ['local', 'hello', '--two', '2', '1']),
            ['1', '2']
        )
        self.assertEqual(
            run_command({'hello': hello}, ['local', 'hello', '1']),
            ['1', 'two_default']
        )


def shell_escape(string):
    for char in ("'",  '$', '`', '\\'):
        string = string.replace(char, '\%s' % char)
    return string


def sudo(user='root', command=None, cwd=None):
    cmd = 'sudo'
    if command is None:
        raise ValueError('command cannot be None')
    if user != 'root':
        cmd += ' -H -u '+user
    if cwd != None:
        cmd += ' ' + cd(cwd, command)
    else:
        cmd += ' ' + command
    return cmd


def cd(directory, command):
    return "/bin/bash -c 'cd " + shell_escape(directory) + " && " + shell_escape(command) +"'"


if __name__ == '__main__':
    unittest.main()
