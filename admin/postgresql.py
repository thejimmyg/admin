import os
from . import sudo


monit_app_template = '''\
 check process {name} with pidfile {install_directory}/var/postgresql/data/postmaster.pid
    # group database
    start program = "{install_directory}/bin/pg_ctl start -D {install_directory}/var/postgresql/data -l {install_directory}/log/postgresql.log"
    stop  program = "{install_directory}/bin/pg_ctl stop -D {install_directory}/var/postgresql/data -l {install_directory}/log/postgresql.log"
 # if failed unixsocket /tmp/.s.PGSQL.5432 protocol pgsql then restart
 # if failed unixsocket /tmp/.s.PGSQL.5432 protocol pgsql then alert
 # if failed host localhost port 5432 protocol pgsql then restart
 # if failed host localhost port 5432 protocol pgsql then alert
 # if 5 restarts within 5 cycles then timeout
'''

def init(run, username, install_directory, name, out=None):
    output = ''
    output += run(sudo(username, 'mkdir -p {install_directory}/var/postgresql/data'.format(install_directory=install_directory, name=name)), out)
    output += run(sudo(username, '{install_directory}/bin/pg_ctl init -D {install_directory}/var/postgresql/data'.format(install_directory=install_directory, name=name)), out)
    return output


def monit(run, username, install_directory, name, out=None):
    output = ''
    output += run(
        'cat - | ' + 
        sudo(
            user=username,
            command='tee {}/etc/monit/conf.d/{}.conf'.format(install_directory, name)
        ),
        out,
        monit_app_template.format(
            install_directory=install_directory,
            username=username,
            name=name,
        ),
    )
    output += run(sudo(username, 'chmod 600 {install_directory}/etc/monit/conf.d/{name}.conf'.format(install_directory=install_directory, name=name)), out)
    return output


actions = {
    'monit': monit,
    'init': init,
}


if __name__ == '__main__':
    import sys
    from . import run_command

    run_command(actions, sys.argv[1:])
