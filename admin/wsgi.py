import os
from . import sudo


monit_app_template = '''\
check process {name} with pidfile {install_directory}/pid/{name}.pid
  start program = "{env_path} PYTHONPATH={install_directory}/var/{name}:$PYTHONPATH {install_directory}/bin/gunicorn --daemon {name} --bind {host}:{port} --pid {install_directory}/pid/{name}.pid --access-logfile {install_directory}/log/{name}.access --error-logfile {install_directory}/log/{name}.error --log-level debug"
  stop program = "/bin/bash -c '/bin/kill `/bin/cat {install_directory}/pid/{name}.pid`'"
  if failed port {port} protocol http with timeout 3 seconds then restart
  # if failed host {host} port {port} protocol http request "/hello" with HOSTHEADER "{hostheader}" then restart
  # if 5 restarts within 5 cycles then alert
'''

  # start program = "{install_directory}/bin/gunicorn --daemon myproject.app:app --bind 127.0.0.1:{port} --worker-class aiohttp.worker.GunicornWebWorker --pid {install_directory}/pid/app.pid --access-logfile {install_directory}/log/app.access --error-logfile {install_directory}/log/app.error --log-level debug"


def pipdeps(run, username, install_directory, requirements, out=None):
    output = ''
    output += run(sudo(username, 'mkdir -p {}/tmp'.format(install_directory)), out)
    with open(requirements, 'r') as fp:
        data = fp.read()
        print(data)
        output += run(
            'cat - | ' +
            sudo(
                user=username,
                command='tee {}/tmp/requirements.txt'.format(install_directory)
            ),
            out,
            data,
        )
    output += run(sudo(username, 'LD_LIBRARY_PATH={}/lib:$LD_LIBRARY_PATH {}/bin/pip3.5 install --upgrade pip'.format(install_directory, install_directory)), out)
    output += run(sudo(username, 'LD_LIBRARY_PATH={}/lib:$LD_LIBRARY_PATH {}/bin/pip3.5 install -r {}/tmp/requirements.txt --upgrade'.format(install_directory, install_directory, install_directory)), out)
    return output


def pip_sdist(run, username, install_directory, sdist_path, ssh_path, out=None):
    output = ''
    filename = os.path.split(sdist_path)[1]
    print(filename)
    run.put(
        localpath = sdist_path,
        filepath = '{ssh_path}/{filename}'.format(ssh_path=ssh_path, filename=filename),
    )
    output += run(
        sudo(
            cwd=install_directory,
            command='mv {ssh_path}/{filename} . ; chown {username} {filename}'.format(username=username, ssh_path=ssh_path, filename=filename),
       ),
       out
    )
    output += run(sudo(username, command='LD_LIBRARY_PATH={}/lib:$LD_LIBRARY_PATH {}/bin/pip3.5 install --upgrade pip'.format(install_directory, install_directory)), out)
    output += run(sudo(username, cwd=install_directory, command='LD_LIBRARY_PATH={}/lib:$LD_LIBRARY_PATH {}/bin/pip3.5 install {} --upgrade'.format(install_directory, install_directory, filename)), out)
    output += run(
        sudo(
            cwd=install_directory,
            command='rm {filename}'.format(username=username, ssh_path=ssh_path, filename=filename),
       ),
       out
    )
    return output


def monit(run, username, install_directory, name, port=8080, host='127.0.0.1', hostheader=None, out=None):
    if hostheader is None:
        hostheader = host
    output = ''
    env_path = '/usr/bin/env'
    output += run(
        'cat - | ' +
        sudo(
            user=username,
            command='tee {}/etc/monit/conf.d/{}.conf'.format(install_directory, name)
        ),
        out,
        monit_app_template.format(
            install_directory=install_directory,
            name=name,
            port=port,
            host=host,
            hostheader=hostheader,
            env_path=env_path,
        ),
    )
    output += run(sudo(username, 'chmod 600 {install_directory}/etc/monit/conf.d/{name}.conf'.format(install_directory=install_directory, name=name)), out)
    print(output)
    return output


def app(run, local_file, username, directory, ssh_path, out=None):
    output = ''
    output += run(sudo(username, 'mkdir -p {}'.format(directory)), out)
    print(local_file, '{ssh_path}/wsgi.tar.gz'.format(ssh_path=ssh_path))
    run.put(
        localpath = local_file,
        filepath = '{ssh_path}/wsgi.tar.gz'.format(ssh_path=ssh_path),
    )
    output += run(
        sudo(
            cwd=directory,
            command='mv {ssh_path}/wsgi.tar.gz . ; chown {username} wsgi.tar.gz'.format(username=username, ssh_path=ssh_path),
       ),
       out
    )
    output += run(
        sudo(
            user=username,
            cwd='{}'.format(directory),
            command='tar -xzf wsgi.tar.gz && rm wsgi.tar.gz'
       ),
       out
    )
    return output


actions = {
    'monit': monit,
    'app': app,
    'pipdeps': pipdeps,
}


if __name__ == '__main__':
    import sys
    from . import run_command

    run_command(actions, sys.argv[1:])
