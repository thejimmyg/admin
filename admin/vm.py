import os
from .build import UBUNTU


VAGRANTFILE_TEMPLATE = '''\
Vagrant.configure(2) do |config|
  config.vm.box = "{box}"
  config.vm.network "private_network", ip: "{private_ip}"
  config.vm.network "forwarded_port", guest: 22, host: {ssh_port}, id: "ssh"
  # config.vm.network "public_network"
  # config.vm.synced_folder "../data", "/vagrant_data"
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "512"
  end
  # config.vm.provision "shell", inline: <<-SHELL
  #   sudo apt-get update
  #   sudo apt-get install -y apache2
  # SHELL
end
'''


def create(run, name, ssh_port=None, private_ip='192.168.33.10',
           img_dir=None, out=None):
    if UBUNTU:
        box = "ubuntu/trusty64"
    else:
        box = "bento/centos-6.7"
    if ssh_port is None:
        ssh_port = int('211'+private_ip.split('.')[-1])
    if img_dir is None:
        img_dir = os.getcwd()
    vagrant_dir = os.path.join(img_dir, name)
    if os.path.exists(vagrant_dir):
        raise VMAlreadyExists(
            'VM {!r} already exists in {!r}'.format(name, img_dir))
    def handle(out):
        output = ''
        output += run(
            'cat - | tee -a Vagrantfile',
            out,
            VAGRANTFILE_TEMPLATE.format(
                ssh_port=ssh_port,
                private_ip=private_ip,
                box=box,
            ).encode('utf8')
        )
        output += run(' '.join(
            ['/usr/local/bin/vagrant', 'up', '--provider', 'virtualbox']), out)
        output += run(' '.join(['/usr/local/bin/vagrant', 'ssh-config']), out)
        return output
    return _prepare_and_run(handle, name, img_dir, out)


def destroy(run, name, img_dir=None, out=None):
    if img_dir is None:
        img_dir = os.getcwd()
    vagrant_dir = os.path.join(img_dir, name)
    if not os.path.exists(vagrant_dir):
        raise NoSuchVM('No such vm {!r} in {!r}'.format(name, img_dir))
    if running(run, name, img_dir, out) == 'True':
        raise CannotDestroyRunningVM(
            'Cannot destory running vm {!r} in {!r}'.format(name, img_dir))
    def handle(out):
        output = ''
        output += run(' '.join(
            ['/usr/local/bin/vagrant', 'destroy', '-f']), out)
        return output
    output = _prepare_and_run(handle, name, img_dir, out)
    assert vagrant_dir
    output += run(' '.join(['rm', '-r', vagrant_dir]), out)


class NoSuchVM(Exception):
    pass


class CannotDestroyRunningVM(Exception):
    pass


class VMAlreadyExists(Exception):
    pass


def running(run, name, img_dir=None, out=None):
    if img_dir is None:
        img_dir = os.getcwd()
    vagrant_dir = os.path.join(img_dir, name)
    cwd = os.getcwd()
    if not os.path.exists(vagrant_dir):
        raise NoSuchVM('No such vm {!r} in {!r}'.format(name, img_dir))
    try:
        os.chdir(vagrant_dir)
        res = run('/usr/local/bin/vagrant status')
        result = 'False'
        if ' running ' in res:
            result = 'True'
        if out is not None:
            out(result)
        return result
    finally:
        os.chdir(cwd)


def start(run, name, img_dir=None, out=None):
    def handle(out):
        return run(' '.join(['/usr/local/bin/vagrant', 'up']), out)
    return _prepare_and_run(handle, name, img_dir, out)


def stop(run, name, img_dir=None, out=None):
    def handle(out):
        return run(' '.join(['/usr/local/bin/vagrant', 'halt']), out)
    return _prepare_and_run(handle, name, img_dir, out)


def ssh(run, name, img_dir=None, out=None):
    def handle(out):
        return run(' '.join(['/usr/local/bin/vagrant', 'ssh-config']), out)
    return _prepare_and_run(handle, name, img_dir, out)


def _prepare_and_run(handle, name, img_dir=None, out=None):
    if img_dir is None:
        img_dir = os.getcwd()
    vagrant_dir = os.path.join(img_dir, name)
    if not os.path.exists(vagrant_dir):
        os.mkdir(vagrant_dir)
    cwd = os.getcwd()
    try:
        os.chdir(vagrant_dir)
        return handle(out)
    finally:
        os.chdir(cwd)


actions = {
    'create': create,
    'destroy': destroy,
    'running': running,
    'stop': stop,
    'start': start,
    'ssh': ssh,
}


if __name__ == '__main__':
    import sys
    from . import run_command

    run_command(actions, sys.argv[1:])
