import os
from . import sudo

#  ./hitch --backend=[127.0.0.1]:8000 ../../key/jimmyg.pem  --daemon

monit_app_template = '''\
check process {name} with pidfile {install_directory}/pid/{name}.pid
  depends on varnish
  start program = "/usr/bin/env LD_LIBRARY_PATH=$LD_LIBRARY_PATH:{install_directory}/lib {install_directory}/sbin/hitch {frontend} --backend=[{backend_host}]:{backend_port} {install_directory}/etc/ssl/{certificate_name} --pidfile {install_directory}/pid/{name}.pid -c 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH' --daemon --syslog"
  stop program = "/bin/bash -c '/bin/kill `/bin/cat {install_directory}/pid/{name}.pid`'"
  #if failed host {host} port {port} protocol https with ssl options {{selfsigned:allow}} with timeout 3 seconds then alert
'''

def monit(run, username, install_directory, name, port=8443, host='127.0.0.1', backend_port=8080, backend_host='localhost', host_header=None, certificate_name='cert', out=None):
    hosts = host.split(',')
    if host_header is None:
        host_header=host[0]
    output = ''
    # if chroot_dir is None:
    #     chroot_dir = '{install_directory}/tmp/chroot'.format(install_directory=install_directory)
    # output += run(
    #     sudo(
    #         user=username,
    #         command='mkdir -p {chroot_dir}/dev'.format(chroot_dir=chroot_dir),
    #     )
    # )
    # output += run(
    #     sudo(
    #         command='mknod {chroot_dir}/dev/null c 1 3'.format(chroot_dir=chroot_dir),
    #     )
    # )
    output += run(
        'cat - | ' +
        sudo(
            user=username,
            command='tee {}/etc/monit/conf.d/{}.conf'.format(install_directory, name)
        ),
        out,
        monit_app_template.format(
            install_directory=install_directory,
            name=name,
            port=port,
            backend_port=backend_port,
            backend_host=backend_host,
            host=hosts[0],
            frontend=' '.join(['--frontend=[{host}]:{port}'.format(host=h, port=port) for h in hosts]),
            host_header=host_header,
            certificate_name=certificate_name,
        ),
    )
    output += run(sudo(username, 'chmod 600 {install_directory}/etc/monit/conf.d/{name}.conf'.format(install_directory=install_directory, name=name)), out)
    return output


def uploadcertificate(run, username, install_directory, pem_filename, certificate_name='cert', out=None):
    output = ''
    cmd = sudo(
        user=username,
        cwd='{}'.format(install_directory),
        command='mkdir -p etc/ssl',
    )
    output += run(cmd, out)
    output += run(
        'cat - | ' + 
        sudo(
            user=username,
            command='tee {}/etc/ssl/{}'.format(install_directory, certificate_name)
        ),
        out,
        open(pem_filename, 'rb').read()
    )
    return output


def selfsignedcertificate(run, username, install_directory, cn, pem_filename=None, subj=None, out=None):
    if pem_filename is None:
        pem_filename = cn+'.pem'
    if subj is None:
        subj = "/C=GB/ST=London/L=London/CN={cn}".format(cn=cn)
    output = ''
    cmd = sudo(
        user=username,
        cwd='{}'.format(install_directory),
        command='mkdir -p tmp; mkdir -p etc/ssl',
    )
    output += run(cmd, out)
    cmd = sudo(
        user=username,
        cwd='{}'.format(install_directory),
        command='openssl req -newkey rsa:2048 -sha256 -keyout "tmp/{cn}.key" -nodes -x509 -days 365 -out "tmp/{cn}.crt" -subj "{subj}"'.format(
            cn=cn,
            subj=subj,
        )
    )
    output += run(cmd, out)
    cmd = sudo(
        user=username,
        cwd='{}'.format(install_directory),
        command='cat "tmp/{cn}.key" "tmp/{cn}.crt" > "etc/ssl/{pem_filename}" '.format(cn=cn, pem_filename=pem_filename)
    )
    output += run(cmd, out)
    cmd = sudo(
        user=username,
        cwd='{}'.format(install_directory),
        command='openssl dhparam -rand - 2048 >> "etc/ssl/{pem_filename}"'.format(pem_filename=pem_filename)
    )
    output += run(cmd, out)
    cmd = sudo(
        user=username,
        cwd='{}'.format(install_directory),
        command='rm "tmp/{cn}.key" "tmp/{cn}.crt"'.format(cn=cn)
    )
    output += run(cmd, out)
    return output


actions = {
    'selfsignedcertificate': selfsignedcertificate,
    'uploadcertificate': uploadcertificate,
    'monit': monit,
}


if __name__ == '__main__':
    import sys
    from . import run_command

    run_command(actions, sys.argv[1:])
