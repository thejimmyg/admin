import os
from . import sudo


monit_app_template = '''\
check process {name} with pidfile {install_directory}/pid/{name}.pid
    start program = "{install_directory}/etc/monit/init.d/{name}.sh start"
    stop program = "{install_directory}/etc/monit/init.d/{name}.sh stop"
    if failed host localhost port {port} then restart
    # if totalmem > 100 Mb then alert
    # if children > 255 for 5 cycles then stop
    # if cpu usage > 95% for 3 cycles then restart
    # if 2 restarts within 5 cycles then alert
'''


redis_template = '''\
#!/bin/sh
#
# Simple Redis init.d script conceived to work on Linux systems
# as it does use of the /proc filesystem.

DIR="{install_directory}"
DAEMON_NAME="{name}"

REDISPORT={port}
EXEC="$DIR/bin/redis-server"
CLIEXEC="$DIR/bin/redis-cli"

PIDFILE="$DIR/pid/{name}.pid"
CONF="$DIR/etc/redis/{name}.conf"

case "$1" in
    start)
        if [ -f $PIDFILE ]
        then
                echo "$PIDFILE exists, process is already running or crashed"
        else
                echo "Starting Redis server..."
                $EXEC $CONF
        fi
        ;;
    stop)
        if [ ! -f $PIDFILE ]
        then
                echo "$PIDFILE does not exist, process is not running"
        else
                PID=$(cat $PIDFILE)
                echo "Stopping ..."
                $CLIEXEC -p $REDISPORT shutdown
                while [ -x /proc/${{PID}} ]
                do
                    echo "Waiting for Redis to shutdown ..."
                    sleep 1
                done

                echo "Redis stopped"
        fi
        ;;
    *)
        echo "Please use start or stop as first argument"
        ;;
esac
'''


def monit(run, username, install_directory, name, port=6379, out=None):
    output = ''
    output += run(sudo(username, 'mkdir -p {install_directory}/pid'.format(install_directory=install_directory)), out)
    output += run(sudo(username, 'mkdir -p {install_directory}/log'.format(install_directory=install_directory)), out)
    output += run(sudo(username, 'mkdir -p {install_directory}/etc/{name}'.format(name=name, install_directory=install_directory)), out)
    output += run(sudo(username, 'mkdir -p {install_directory}/var/{name}/{port}'.format(name=name, install_directory=install_directory, port=port)), out)
    output += run(
        'cat - | ' + 
        sudo(
            user=username,
            command='tee {}/etc/redis/{}.conf'.format(install_directory, name)
        ),
        out,
        '''\
# Redis config
daemonize yes
pidfile /opt/jimmyg/platform/pid/{name}.pid
logfile /opt/jimmyg/platform/log/{name}.log
dir /opt/jimmyg/platform/var/{name}/{port}
port {port}
'''.format(
            install_directory=install_directory,
            name=name,
            port=port,
        ),
    )
    output += run(
        'cat - | ' + 
        sudo(
            user=username,
            command='tee {}/etc/monit/conf.d/{}.conf'.format(install_directory, name)
        ),
        out,
        monit_app_template.format(
            install_directory=install_directory,
            name=name,
            port=port,
        ),
    )
    output += run(sudo(username, 'chmod 600 {install_directory}/etc/monit/conf.d/{name}.conf'.format(install_directory=install_directory, name=name)), out)
    output += run(
        'cat - | ' + 
        sudo(
            user=username,
            command='tee {}/etc/monit/init.d/{}.sh'.format(install_directory, name)
        ),
        out,
        redis_template.format(
            install_directory=install_directory,
            name=name,
            port=port,
        ),
    )
    output += run(sudo(username, 'chmod +x {install_directory}/etc/monit/init.d/{name}.sh'.format(install_directory=install_directory, name=name)), out)
    return output


actions = {
    'monit': monit,
}


if __name__ == '__main__':
    import sys
    from . import run_command

    run_command(actions, sys.argv[1:])
