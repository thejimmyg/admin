def echo(run, one, two, out=None):
    if out is None:
        out = print
    print(one, two)

actions = {
    'echo': echo,
}

if __name__ == '__main__':
    import sys
    from admin import run_command

    run_command(actions, sys.argv[1:])
