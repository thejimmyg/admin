import os
from . import sudo


elasticsearch_script = '''\
#!/bin/sh

. /lib/lsb/init-functions

DESC="Elasticsearch Server"
INSTALL_DIR={install_directory}
JAVA_HOME=$INSTALL_DIR/opt/java/jre1.8.0_101/
ES_HOME=$INSTALL_DIR/opt/elasticsearch/elasticsearch-2.4.0
PID_FILE=$INSTALL_DIR/pid/elasticsearch.pid

case "$1" in
  start)
    if [ -n "$pid" ] ; then
        log_begin_msg "Already running."
        log_end_msg 0
        exit 0
    fi
    JAVA_HOME=$JAVA_HOME $ES_HOME/bin/elasticsearch -d -p $PID_FILE --default.path.home=$ES_HOME --default.path.logs=$INSTALL_DIR/log/elasticsearch.log --default.path.data=/opt/jimmyg/platform/var/elasticsearch-2.4.0 --default.path.work=

    return=$?
    if [ $return -eq 0 ]
    then
        i=0
        timeout=10
        # Wait for the process to be properly started before exiting
        until {{ cat "$PID_FILE" | xargs kill -0; }} >/dev/null 2>&1
        do
            sleep 1
            i=$(($i + 1))
            [ $i -gt $timeout ] && log_end_msg 1
        done
    else
        log_end_msg $return
    fi
    ;;
  stop)
    log_daemon_msg "Stopping $DESC"

    if [ -f "$PID_FILE" ]; then
        start-stop-daemon --stop --pidfile "$PID_FILE" --retry=TERM/20/KILL/5 >/dev/null
        if [ $? -eq 1 ]; then
            log_progress_msg "$DESC is not running but pid file exists, cleaning up"
        elif [ $? -eq 3 ]; then
            PID="`cat $PID_FILE`"
            log_failure_msg "Failed to stop $DESC (pid $PID)"
            exit 1
        fi
        rm -f "$PID_FILE"
    else
        log_progress_msg "(not running)"
    fi
    log_end_msg 0
    ;;
  restart|force-reload)
    if [ -f "$PID_FILE" ]; then
        $0 stop
        sleep 1
    fi
    $0 start
    ;;
  *)
    log_success_msg "Usage: $0 {{start|stop|restart|force-reload|status}}"
    exit 1
    ;;
esac

exit 0
'''

monit_app_template = '''\
check process {name} with pidfile {install_directory}/pid/{name}.pid
   start program = "{install_directory}/etc/monit/init.d/elasticsearch.sh start"
   stop program =  "{install_directory}/etc/monit/init.d/elasticsearch.sh stop"
'''


def init(run, username, install_directory, name, out=None):
    output = ''
    output += run(sudo(username, 'mkdir -p {install_directory}/var/elasticsearch-2.4.0'.format(install_directory=install_directory, name=name)), out)
    return output


def monit(run, username, install_directory, name, out=None):
    output = ''
    output += run(
        'cat - | ' + 
        sudo(
            user=username,
            command='tee {}/etc/monit/conf.d/{}.conf'.format(install_directory, name)
        ),
        out,
        monit_app_template.format(
            install_directory=install_directory,
            username=username,
            name=name,
        ),
    )
    output += run(sudo(username, 'chmod 600 {install_directory}/etc/monit/conf.d/{name}.conf'.format(install_directory=install_directory, name=name)), out)
    output += run(
        'cat - | ' +
        sudo(
            user=username,
            command='tee {}/etc/monit/init.d/elasticsearch.sh'.format(install_directory, name)
        ),
        out,
        elasticsearch_script.format(
            install_directory=install_directory,
            username=username,
            name=name,
            # port=port,
            # host=host,
            # module=module,
        ),
    )
    output += run(sudo(username, 'chmod +x {install_directory}/etc/monit/init.d/elasticsearch.sh'.format(install_directory=install_directory, name=name)), out)
    return output


actions = {
    'monit': monit,
    'init': init,
}


if __name__ == '__main__':
    import sys
    from . import run_command

    run_command(actions, sys.argv[1:])
