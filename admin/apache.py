import os
from . import sudo


apache_config = '''\
ServerRoot "{install_directory}"
Listen 0.0.0.0:{frontend_port}

LoadModule authn_file_module modules/mod_authn_file.so
LoadModule authn_core_module modules/mod_authn_core.so
LoadModule authz_host_module modules/mod_authz_host.so
LoadModule authz_groupfile_module modules/mod_authz_groupfile.so
LoadModule authz_user_module modules/mod_authz_user.so
LoadModule authz_core_module modules/mod_authz_core.so
LoadModule access_compat_module modules/mod_access_compat.so
LoadModule auth_basic_module modules/mod_auth_basic.so
LoadModule reqtimeout_module modules/mod_reqtimeout.so
LoadModule filter_module modules/mod_filter.so
LoadModule mime_module modules/mod_mime.so
LoadModule log_config_module modules/mod_log_config.so
LoadModule env_module modules/mod_env.so
LoadModule headers_module modules/mod_headers.so
LoadModule setenvif_module modules/mod_setenvif.so
LoadModule version_module modules/mod_version.so
LoadModule unixd_module modules/mod_unixd.so
LoadModule status_module modules/mod_status.so
LoadModule autoindex_module modules/mod_autoindex.so
LoadModule dir_module modules/mod_dir.so
LoadModule alias_module modules/mod_alias.so
LoadModule wsgi_module modules/mod_wsgi.so

<IfModule unixd_module>
User {username}
Group {username}
</IfModule>

ServerAdmin james@pythonweb.org
ServerName {frontend_host}:80

<Directory />
    AllowOverride none
    Require all denied
</Directory>

DocumentRoot "{install_directory}/htdocs"
<Directory "{install_directory}/htdocs">
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>

<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

<Files ".ht*">
    Require all denied
</Files>

ErrorLog "logs/error_log"
LogLevel warn

<IfModule log_config_module>
    LogFormat "%h %l %u %t \\"%r\\" %>s %b \\"%{{Referer}}i\\" \\"%{{User-Agent}}i\\"" combined
    LogFormat "%h %l %u %t \\"%r\\" %>s %b" common
    <IfModule logio_module>
      LogFormat "%h %l %u %t \\"%r\\" %>s %b \\"%{{Referer}}i\\" \\"%{{User-Agent}}i\\" %I %O" combinedio
    </IfModule>
    CustomLog "logs/access_log" common
</IfModule>


<IfModule mime_module>
    TypesConfig conf/mime.types
    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz
</IfModule>

# Configure mod_proxy_html to understand HTML4/XHTML1
<IfModule proxy_html_module>
Include conf/extra/proxy-html.conf
</IfModule>

<VirtualHost *:{frontend_port}>
  ServerName {frontend_host}
  ServerAdmin james@pythonweb.org
  CustomLog logs/{frontend_host}-access_log common
  ErrorLog logs/{frontend_host}-error_log

  LogLevel info

  WSGIDaemonProcess {frontend_host} user={username} group={username} processes=2 threads=25 python-path={user_directory}/app
  WSGIProcessGroup {frontend_host}
  WSGIScriptAlias / {user_directory}/app/{module}/wsgi.py process-group={frontend_host}

  <Directory {user_directory}/app/{module}>
    Require all granted
  </Directory>

  Alias /media/ {user_directory}/media/
  Alias /static/ {user_directory}/static/

  <Directory {user_directory}/media/>
    Require all granted
  </Directory>

  <Directory {user_directory}/static/>
    Require all granted
  </Directory>

</VirtualHost>
'''


monit_apache_template = '''\
check process apache with pidfile {install_directory}/logs/httpd.pid
  start program = "{install_directory}/etc/monit/init.d/apache.sh"
  stop program = "{install_directory}/bin/apachectl -k stop" 
'''


apache_script = '''\
#!/bin/bash

source /opt/jimmyg/config/production.config
/opt/jimmyg/platform/bin/apachectl -k start
'''


wsgi_script = '''\
def application(environ, start_response):
    status = '200 OK'
    output = b'Hello World!'
    response_headers = [('Content-type', 'text/plain'),
                        ('Content-Length', str(len(output)))]
    start_response(status, response_headers)
    return [output]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    httpd = make_server('', 8000, application)
    print("Serving on port 8000...")
    httpd.serve_forever()
'''


def monit(run, username, install_directory, name, frontend_host, frontend_port, user_directory, out=None):
    output = ''
    output += run(sudo(username, 'mkdir -p {install_directory}/etc/apache'.format(install_directory=install_directory)))
    output += run(sudo(username, 'mkdir -p {user_directory}/app/{username}'.format(user_directory=user_directory, username=username)))
    output += run(sudo(username, 'mkdir -p {user_directory}/config'.format(user_directory=user_directory)))
    output += run(sudo(username, 'mkdir -p {user_directory}/bin'.format(user_directory=user_directory)))
    output += run(sudo(username, 'mkdir -p {user_directory}/static'.format(user_directory=user_directory)))
    output += run(sudo(username, 'mkdir -p {user_directory}/media'.format(user_directory=user_directory)))
    output += run(sudo(username, 'cp {install_directory}/conf/httpd.conf {install_directory}/conf/httpd.conf.orig'.format(install_directory=install_directory)))
    output += run(
        'cat - | ' +
        sudo(
            user=username,
            command='tee {}/conf/httpd.conf'.format(install_directory)
        ),
        out,
        apache_config.format(
            username=username,
            user_directory=user_directory,
            install_directory=install_directory,
            frontend_host=frontend_host,
            frontend_port=frontend_port,
            module=username,
        )
    )
    output += run(
        'cat - | ' + 
        sudo(
            user=username,
            command='tee {}/etc/monit/conf.d/{}.conf'.format(install_directory, name)
        ),
        out,
        monit_apache_template.format(
            install_directory=install_directory,
        ),
    )
    output += run(
        'cat - | ' + 
        sudo(
            user=username,
            command='tee {}/etc/monit/init.d/apache.sh'.format(install_directory)
        ),
        out,
        apache_script.format(
            install_directory=install_directory,
        ),
    )
    output += run(
        'cat - | ' + 
        sudo(
            user=username,
            command='tee {user_directory}/app/{username}/wsgi.py'.format(user_directory=user_directory, username=username)
        ),
        out,
        wsgi_script.format(
            install_directory=install_directory,
        ),
    )
    output += run(sudo(username, 'chmod +x {install_directory}/etc/monit/init.d/apache.sh'.format(install_directory=install_directory)), out)
    return output


actions = {
    'monit': monit,
}


if __name__ == '__main__':
    import sys
    from . import run_command

    run_command(actions, sys.argv[1:])
