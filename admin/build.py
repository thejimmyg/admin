import os
from . import sudo


UBUNTU = True


def ping(run, out=None):
    return run('echo "Pong"', out)


def whoami(run, out=None):
    return run('whoami', out).strip()


def home(run, out=None):
    return run('echo $HOME', out).strip()


def useradd(run, username, home_directory=None, out=None):
    cmd = 'sudo useradd'
    if home_directory:
        cmd += ' -m -d '+home_directory
    cmd += ' -s /bin/bash'
    cmd += ' ' + username
    return run(cmd, out)


def tarball(run, username, install_directory, build_directory, ssh_user='vagrant', ssh_path='/home/vagrant', local_path='build.tar.gz', out=None):
    output = ''
    output += run(
        sudo(
            user=username,
            cwd='{}'.format(build_directory),
            command='tar --exclude="{directory}/build" --exclude="{directory}/download" -czvf build.tar.gz {directory}'.format(directory=os.path.relpath(install_directory, build_directory)),
       ),
       out
    )
    output += run(
        sudo(
            user='root',
            cwd='{}'.format(install_directory),
            command='mv {build_directory}/build.tar.gz {ssh_path}/ ; chown {ssh_user} {ssh_path}/build.tar.gz'.format(ssh_path=ssh_path, ssh_user=ssh_user, build_directory=build_directory),
       ),
       out
    )
    run.get(
        file_path = '{ssh_path}/build.tar.gz'.format(ssh_path=ssh_path),
        local_path = local_path,
    )
    return output


def varnish(run, username, install_directory, out=None):
    output = ''
    if UBUNTU:
        # Ubuntu 14.04
        output += run(sudo('root', 'apt-get install -y automake autotools-dev libedit-dev libjemalloc-dev libncurses-dev libpcre3-dev libtool pkg-config python-docutils python-sphinx graphviz curl'), out)
    else:
        # CentOS 6.7
        output += run(sudo('root', 'yum install -y autoconf automake jemalloc-devel libedit-devel libtool ncurses-devel pcre-devel pkgconfig python-docutils python-sphinx'), out)
    output += run(sudo(username, 'mkdir -p {}/download'.format(install_directory)), out)
    output += run(sudo(username, 'mkdir -p {}/build'.format(install_directory)), out)
    output += run(sudo(username, 'curl -o {}/download/varnish-4.1.2.tar.gz https://repo.varnish-cache.org/source/varnish-4.1.2.tar.gz'.format(install_directory)), out)
    output += run(sudo(username, 'tar zxfv {}/download/varnish-4.1.2.tar.gz -C {}/build'.format(install_directory, install_directory)), out)
    output += run(
        sudo(
            user=username,
            cwd='{}/build/varnish-4.1.2'.format(install_directory),
            command='./autogen.sh && ./configure --prefix={}/ && make && make install'.format(install_directory),
       ),
       out
    )
    output += run(
        sudo(
            command=(
                'cp /usr/lib/x86_64-linux-gnu/libjemalloc.so.1 {install_directory}/lib/'
           ).format(install_directory=install_directory)
        ),
        out
    )
    output += run(
        sudo(
            command=(
                'chown {username}:{username} {install_directory}/lib/libjemalloc.so.1'
           ).format(install_directory=install_directory, username=username)
        ),
        out
    )
    return output


def hitch(run, username, install_directory, out=None):
    output = ''
    output += run(sudo('root', 'apt-get install -y build-essential libev-dev libssl-dev automake python-docutils flex bison pkg-config'), out)
    output += run(sudo(username, 'mkdir -p {}/download'.format(install_directory)), out)
    output += run(sudo(username, 'mkdir -p {}/build'.format(install_directory)), out)
    output += run(sudo(username, 'curl -o {}/download/hitch-1.2.0.tar.gz https://hitch-tls.org/source/hitch-1.2.0.tar.gz'.format(install_directory)), out)
    output += run(sudo(username, 'tar zxfv {}/download/hitch-1.2.0.tar.gz -C {}/build'.format(install_directory, install_directory)), out)
    output += run(
        sudo(
            user=username,
            cwd='{}/build/hitch-1.2.0'.format(install_directory),
            command='./configure --prefix={}/ && make && make install'.format(install_directory),
        ),
        out
    )
    output += run(
        sudo(
            command=(
                'mkdir -p {install_directory}/lib'
           ).format(install_directory=install_directory)
        ),
        out
    )
    output += run(
        sudo(
            command=(
                'cp /usr/lib/x86_64-linux-gnu/libev.so.4 {install_directory}/lib/'
           ).format(install_directory=install_directory)
        ),
        out
    )
    output += run(
        sudo(
            command=(
                'chown {username}:{username} {install_directory}/lib/libev.so.4'
           ).format(install_directory=install_directory, username=username)
        ),
        out
    )
    return output


def redis(run, username, install_directory, out=None):
    output = ''
    output += run(sudo(username, 'mkdir -p {}/download'.format(install_directory)), out)
    output += run(sudo(username, 'mkdir -p {}/build'.format(install_directory)), out)
    output += run(sudo(username, 'curl -o {}/download/redis-3.2.0.tar.gz http://download.redis.io/releases/redis-3.2.0.tar.gz'.format(install_directory)), out)
    output += run(sudo(username, 'tar zxfv {}/download/redis-3.2.0.tar.gz -C {}/build'.format(install_directory, install_directory)), out)
    output += run(
        sudo(
            user=username,
            cwd='{}/build/redis-3.2.0'.format(install_directory),
            command='make && make test && cp src/redis-server {}/bin/ && cp src/redis-cli {}/bin/'.format(install_directory, install_directory),
       ),
       out
    )
    return output


def postgresql_dev(run, out=None):
    output = ''
    output += run(sudo('root', 'apt-get install -y libpq-dev'), out)
    return output


def postgresql(run, username, install_directory, out=None):
    output = ''
    if UBUNTU:
        # Ubuntu 14.04
        output += run(sudo('root', 'apt-get install -y libreadline-dev'), out)
    else:
        # CentOS 6.7
        output += run(sudo('root', 'yum install -y readline-devel'), out)
    output += run(sudo(username, 'mkdir -p {}/download'.format(install_directory)), out)
    output += run(sudo(username, 'mkdir -p {}/build'.format(install_directory)), out)
    output += run(sudo(username, 'curl -o {}/download/postgresql-9.5.3.tar.gz https://ftp.postgresql.org/pub/source/v9.5.3/postgresql-9.5.3.tar.gz'.format(install_directory)), out)

    output += run(sudo(username, 'tar zxfv {}/download/postgresql-9.5.3.tar.gz -C {}/build'.format(install_directory, install_directory)), out)
    output += run(
        sudo(
            user=username,
            cwd='{}/build/postgresql-9.5.3'.format(install_directory),
            command='./configure --prefix={}/ && make && make install && make check'.format(install_directory),
       ),
       out
    )
    return output


def java8(run, username, install_directory, out=None):
    output = ''
    output += run(sudo(username, 'mkdir -p {}/opt/java'.format(install_directory)), out)
    output += run(
        sudo(
            user=username,
            cwd='{}/opt/java'.format(install_directory),
            command='curl -L -C - -b "oraclelicense=accept-securebackup-cookie" -O http://download.oracle.com/otn-pub/java/jdk/8u101-b13/jre-8u101-linux-x64.tar.gz',
        ),
        out
    )
    output += run(
        sudo(
            user=username,
            cwd='{}/opt/java'.format(install_directory),
            command='tar zxfv jre-8u101-linux-x64.tar.gz && rm jre-8u101-linux-x64.tar.gz'.format(install_directory)
        ),
        out
    )
    return output


def elasticsearch(run, username, install_directory, out=None):
    output = ''
    output += run(sudo(username, 'mkdir -p {}/opt/elasticsearch'.format(install_directory)), out)
    output += run(
        sudo(
            user=username,
            cwd='{}/opt/elasticsearch'.format(install_directory),
            command='curl -O https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/tar/elasticsearch/2.4.0/elasticsearch-2.4.0.tar.gz',
        ),
        out
    )
    output += run(
        sudo(
            user=username,
            cwd='{}/opt/elasticsearch'.format(install_directory),
            command='tar zxfv elasticsearch-2.4.0.tar.gz && rm elasticsearch-2.4.0.tar.gz'.format(install_directory)
        ),
        out
    )
    return output


def python3(run, username, install_directory, out=None):
    output = ''
    # output += run(sudo('root', 'apt-get install -y build-essential libbz2-dev libsqlite3-dev libssl-dev libreadline6-dev libncurses5-dev liblzma-dev libgdbm-dev tk8.5-dev zlib1g-dev'), out)
    # http://blog.dscpl.com.au/2015/06/installing-custom-python-version-into.html
    output += run(sudo('root', 'apt-get install -y build-essential libbz2-dev libsqlite3-dev libssl-dev libreadline-dev libncurses-dev liblzma-dev libgdbm-dev tk-dev   libc6-dev libdb-dev libexpat1-dev  libffi-dev libtinfo-dev zlib1g-dev'), out)
    output += run(sudo(username, 'mkdir -p {}/download'.format(install_directory)), out)
    output += run(sudo(username, 'mkdir -p {}/build'.format(install_directory)), out)
    output += run(sudo(username, 'curl -o {}/download/Python-3.5.2.tgz https://www.python.org/ftp/python/3.5.2/Python-3.5.2.tgz'.format(install_directory)), out)
    output += run(sudo(username, 'tar zxfv {}/download/Python-3.5.2.tgz -C {}/build'.format(install_directory, install_directory)), out)
    output += run(
        sudo(
            user=username,
            cwd='{}/build/Python-3.5.2'.format(install_directory),
            command='./configure --enable-shared --prefix={}/ && make && make install'.format(install_directory),
       ),
       out
    )
    return output


def monit(run, username, install_directory, out=None):
    output = []
    output += run(sudo(username, 'mkdir -p {}/etc/monit/conf.d'.format(install_directory)), out)
    if UBUNTU:
        # Ubuntu 14.04
        output += run(sudo('root', 'apt-get install -y build-essential libssl-dev libpq-dev'), out)
    else:
        # CentOS 6.7
        output += run(sudo('root', 'yum groupinstall -y "Development tools"'), out)
        output += run(sudo('root', 'yum install -y openssl-devel postgresql-devel'), out)
    output += run(sudo(username, 'mkdir -p {}/download'.format(install_directory)), out)
    output += run(sudo(username, 'mkdir -p {}/build'.format(install_directory)), out)
    output += run(sudo(username, 'curl -o {}/download/monit-5.17.1.tar.gz https://mmonit.com/monit/dist/monit-5.17.1.tar.gz'.format(install_directory)), out)
    output += run(sudo(username, 'tar zxfv {}/download/monit-5.17.1.tar.gz -C {}/build'.format(install_directory, install_directory)), out)
    output += run(
        sudo(
            user=username,
            cwd='{}/build/monit-5.17.1'.format(install_directory),
            command='./configure --prefix={}/ --without-pam && make && make install'.format(install_directory),
       ),
       out
    )
    return output


def apache(run, username, install_directory, out=None):
    output = ''
    output += run(sudo('root', 'apt-get install -y build-essential libtool automake'), out)
    output += run(sudo(username, 'mkdir -p {}/download'.format(install_directory)), out)
    output += run(sudo(username, 'mkdir -p {}/build'.format(install_directory)), out)
    output += run(sudo(username, 'curl -o {}/download/httpd-2.4.23.tar.gz http://www.mirrorservice.org/sites/ftp.apache.org//httpd/httpd-2.4.23.tar.gz'.format(install_directory)), out)
    output += run(sudo(username, 'tar zxfv {}/download/httpd-2.4.23.tar.gz -C {}/build'.format(install_directory, install_directory)), out)
    output += run(sudo(username, 'curl -o {}/download/apr-1.5.2.tar.gz http://www.mirrorservice.org/sites/ftp.apache.org//apr/apr-1.5.2.tar.gz'.format(install_directory)), out)
    output += run(sudo(username, 'tar zxfv {}/download/apr-1.5.2.tar.gz -C {}/build'.format(install_directory, install_directory)), out)
    output += run(sudo(username, 'curl -o {}/download/apr-util-1.5.4.tar.gz http://www.mirrorservice.org/sites/ftp.apache.org//apr/apr-util-1.5.4.tar.gz'.format(install_directory)), out)
    output += run(sudo(username, 'tar zxfv {}/download/apr-util-1.5.4.tar.gz -C {}/build'.format(install_directory, install_directory)), out)
    output += run(sudo(username, 'mv {}/build/apr-1.5.2 {}/build/httpd-2.4.23/srclib/apr'.format(install_directory, install_directory)), out)
    output += run(sudo(username, 'mv {}/build/apr-util-1.5.4 {}/build/httpd-2.4.23/srclib/apr-util'.format(install_directory, install_directory)), out)
    output += run(
        sudo(
            user=username,
            cwd='{}/build/httpd-2.4.23'.format(install_directory),
            command='./configure --with-included-apr --prefix={} && make && make install'.format(install_directory),
       ),
       out
    )
    return output


def mod_wsgi(run, username, install_directory, out=None):
    output = ''
    # output += run(sudo('root', 'apt-get install -y build-essential libtool automake'), out)
    output += run(sudo(username, 'mkdir -p {}/download'.format(install_directory)), out)
    output += run(sudo(username, 'mkdir -p {}/build'.format(install_directory)), out)
    output += run(sudo(username, 'curl -o {}/download/mod_wsgi-4.5.6.tar.gz https://codeload.github.com/GrahamDumpleton/mod_wsgi/tar.gz/4.5.6'.format(install_directory)), out)
    output += run(sudo(username, 'tar zxfv {}/download/mod_wsgi-4.5.6.tar.gz -C {}/build'.format(install_directory, install_directory)), out)
    output += run(
        sudo(
            user=username,
            cwd='{}/build/mod_wsgi-4.5.6'.format(install_directory),
            command='PATH=$PATH:{install_directory}/bin LD_LIBRARY_PATH=$LD_LIBRARY_PATH:{install_directory}/lib ./configure --with-apxs={install_directory}/bin/apxs --with-python={install_directory}/bin/python3.5 --prefix={install_directory}/ && make && make install'.format(install_directory=install_directory),
       ),
       out
    )
    return output


actions = {
    'ping': ping,
    'useradd': useradd,
    'python3': python3,
    'java8': java8,
    'elasticsearch': elasticsearch,
    'postgresql': postgresql,
    'postgresql_dev': postgresql_dev,
    'redis': redis,
    'varnish': varnish,
    'apache': apache,
    'mod_wsgi': mod_wsgi,
    'hitch': hitch,
    'monit': monit,
    'tarball': tarball,
    'whoami': whoami,
    'home': home,
}


if __name__ == '__main__':
    import sys
    from . import run_command

    run_command(actions, sys.argv[1:])
