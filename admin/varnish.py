import os
from . import sudo


varnish_config = '''\
vcl 4.0;

import std;

backend default {{
    .host = "127.0.0.1";
    .port = "{backend_port}";
}}


sub vcl_recv {{
  if (client.ip != "127.0.0.1" ) {{
    set req.http.x-redir = "https://{frontend_host}" + req.url;
    return (synth(750, ""));
  }}
}}


sub vcl_synth {{
  # Listen to 750 status from vcl_recv.
  if (resp.status == 750) {{
    set resp.status = 302;
    set resp.http.Location = req.http.x-redir;
    return(deliver);
  }}
}}


// sub vcl_backend_response {{
//     if (beresp.ttl <= 0s ||
//         beresp.http.Set-Cookie ||
//         beresp.http.Surrogate-control ~ "no-store" ||
//         (!beresp.http.Surrogate-Control &&
//         beresp.http.Cache-Control ~ "no-cache|no-store|private") ||
//         beresp.http.Vary == "*") {{
//         /*
//          * Mark as "Hit-For-Pass" for the next 2 minutes
//          */
//         set beresp.ttl = 10s;
//         set beresp.uncacheable = true;
//     }}
//     return (deliver);
// }}
'''


varnish_script = '''\
#!/bin/bash

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:{install_directory}/lib:{install_directory}/lib/varnish/ \\
  {install_directory}/sbin/varnishd \\
    -a {frontend_ip}:{frontend_port} \\
    -a 127.0.0.1:{frontend_port} \\
    -s default=malloc,256m \\
    -f {install_directory}/etc/varnish/varnish.conf \\
    -P {install_directory}/pid/varnish.pid

    # -s static=file,{install_directory}/var/varnish_storage.bin,100M \\
'''


monit_varnish_template = '''\
check process varnish with pidfile {install_directory}/pid/varnish.pid
  start program = "/opt/jimmyg/platform/etc/monit/init.d/varnish.sh"
  stop program = "/bin/bash -c '/bin/kill `/bin/cat {install_directory}/pid/varnish.pid`'" 
'''


def monit(run, username, install_directory, name, frontend_host, frontend_ip, frontend_port, backend_port, out=None):
    output = ''
    output += run(sudo(username, 'mkdir -p {install_directory}/etc/varnish'.format(install_directory=install_directory)))
    output += run(
        'cat - | ' +
        sudo(
            user=username,
            command='tee {}/etc/varnish/varnish.conf'.format(install_directory)
        ),
        out,
        varnish_config.format(
            frontend_host=frontend_host,
            backend_port=backend_port
        )
    )
    output += run(
        'cat - | ' + 
        sudo(
            user=username,
            command='tee {}/etc/monit/conf.d/{}.conf'.format(install_directory, name)
        ),
        out,
        monit_varnish_template.format(
            install_directory=install_directory,
        ),
    )
    output += run(
        'cat - | ' + 
        sudo(
            user=username,
            command='tee {}/etc/monit/init.d/varnish.sh'.format(install_directory)
        ),
        out,
        varnish_script.format(
            install_directory=install_directory,
            frontend_ip=frontend_ip,
            frontend_port=frontend_port,
        ),
    )
    output += run(sudo(username, 'chmod +x {install_directory}/etc/monit/init.d/varnish.sh'.format(install_directory=install_directory)), out)
    return output


actions = {
    'monit': monit,
}


if __name__ == '__main__':
    import sys
    from . import run_command

    run_command(actions, sys.argv[1:])
