Admin
+++++

Introduction
============

Admin is a tool for performing admin tasks on a project. Including:

* Creating built environments
* Provisioning virtual machines
* Deploying to servers
* Running tests

The model for achieving this is very simple. You can specify `local` and/or the
hostname (as defined in your `~/.ssh/config` file) of the machine(s) you want
to connect to. You then run commands against those machines.

Runners
-------

Internally, Admin creates a *runner* that will send any *command* you give it
to all the machines you've specified. For `local`, a subprocess-based runner is
used, for SSH hosts, an SSH-based runner is used.

Both runners expect all input to be a string, and all output to be strings. The
runners combine stdout and stderr and decode everything as UTF-8. This means
that you can't use admin for sending binary data backwards and forwards, just
for executing string commands.

Builds
------

For deployment, we take the view that all dependencies should just be unzipped
into the right location, and that installing anything is a mistake.

In order to create the builds you'll therefore need a virtual machine running
the same operating system as the production system, and have everything
installed as the same user in the same location. The Admin tool helps with
this.

Standard Services
-----------------

There are also a standard set of services we'll need. These are:

monit
    For monitoring and restarting all the other tools
filesystem
    For large files
postgresql
    For all data except files
redis
    For a queue for celery and for in-memory caching
varnish
    For HTTP caching
hitch
    For TLS termination

Within this setup we provide the ability to add Django WSGI projects:

* Django projects (including wagtail)

Getting Started
===============

Firstly, the box you are running the commands from needs:

* Python 3.5
* A copy of the `admin` package

If you plan to use VMs for build boxes, you will also need:

* VirtualBox
* Vagrant

Once all these are in place, you can run:

::

    virtualenv -p $(which python3) .ve
    . .ve/bin/activate
    pip install -r requirements.txt

At this point, you can use the Admin package to perform useful work.


Using Admin 
===========

In Admin, *actions* are functions that run *commands* against *machines* to do
useful things. They are grouped into *modules*.

There are three ways you can invoke an action, and all are equivalent. You can:

* Call the actions directly as normal Python functions
* Import the module and invoke the command from the command line
* Use the ..py:function`cmd()` function to run the command for you, using the
  same syntax you would use on the command line (internally the
  ..py:function`cmd()` function uses the ..py:module`shelex` module to parse
  commands)

For the purposes of demonstrating the different approaches there is an `echo`
action in the `echo` module. This is unique amongst actions in that it ignores
the runner, and just prints its arguments.

These three approaches are equivalent ways of running the `echo` action:

.. doctest ::

    >>> from admin import cmd
    >>> cmd('echo local echo "To boldly" go\ where\ no-one\ has\ gone\ before')
    To boldly go where no-one has gone before

::

    $ python -m admin.echo local echo "To boldly" go\ where\ no-one\ has\ gone\ before'

.. doctest ::

    >>> from admin.echo import echo
    >>> from admin import LocalRunner
    >>> run = LocalRunner()
    >>> echo(run, 'To boldly', 'go where no-one has gone before', out=print)
    To boldly go where no-one has gone before

In all three cases you choose the module, the hosts, the action and then
specify arguments. You can choose to specify parameters by their position or
their name, just like in Python. For example this works fine too:

.. doctest::

    >>> cmd('echo local echo --one "To boldly" --two go\ where\ no-one\ has\ gone\ before')
    To boldly go where no-one has gone before

In the documentation we'll generally use the `cmd()` variant but you should
choose the one you prefer.
