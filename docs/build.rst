Build
+++++

Now you can run build commands, specifying the SSH name of the host to connect to. For example:

.. doctest ::

    >>> from admin import cmd
    >>> # cmd('build build2 ping', out=print)

Now create a new user:

.. doctest ::

    >>> # cmd('build build2 useradd --home-directory /home/threeaims01 threeaims01', out=print)

and install Python3 and Monit into that user's home directory:

.. doctest ::

    >>> # cmd('build build2 python3 --home-directory /home/threeaims01 threeaims01', out=print)
    >>> # cmd('build build2 monit --home-directory /home/threeaims01 threeaims01', out=print)

Create a tarball:

.. doctest ::

    >>> # cmd('build build2 tarball --home-directory /home/threeaims01 threeaims01')

