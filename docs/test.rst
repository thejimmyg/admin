Test
++++

You'll need to add a SSH config entry for a VM named `build1` before you can
run the tests.

Add this to ``~/.ssh/config``, replacing ``<path>`` with the path to the
``admin`` package source code containing ``behave.ini``:

::

    Host build1
      HostName 127.0.0.1
      User vagrant
      Port 21111
      UserKnownHostsFile /dev/null
      StrictHostKeyChecking no
      PasswordAuthentication no
      IdentityFile "<path>/admin/test_dir/build1/.vagrant/machines/default/virtualbox/private_key"
      IdentitiesOnly yes
      LogLevel FATAL

    Host build2
      HostName 127.0.0.1
      User vagrant
      Port 21112
      UserKnownHostsFile /dev/null
      StrictHostKeyChecking no
      PasswordAuthentication no
      IdentityFile "<path>/admin/test_dir/build2/.vagrant/machines/default/virtualbox/private_key"
      IdentitiesOnly yes
      LogLevel FATAL

    Host build3
      HostName 127.0.0.1
      User vagrant
      Port 21113
      UserKnownHostsFile /dev/null
      StrictHostKeyChecking no
      PasswordAuthentication no
      IdentityFile "<path>/admin/test_dir/build3/.vagrant/machines/default/virtualbox/private_key"
      IdentitiesOnly yes
      LogLevel FATAL


You'll also need to install admin and the test dependencies:

::

    virtualenv -p $(which python3) .ve
    . .ve/bin/activate
    pip install -r requirements.txt -r requirements/test.txt

On Mac OS X you'll also need:

::

     brew install phantomjs automake


Then behave tests require a `behave.ini` file:

::

    [behave.userdata]
    test_host = http://localhost:8000
    # Can use chrome here too
    # driver = phantomjs

Now stop any VMs in the VirtualBox GUI and remove all data associated with
them.

Make sure there is nothing left from previous runs with:

::

    rm -rf test_dir

Then run the tests with:

::

    behave --no-capture

If you forget the ``--no-capture`` you won't see output when long commands
(like making a VM) run. If you forget to delete the contents of ``test_dir``
then some of the tests will fail complaining the VM already exists.


Other Tests
===========

Other tests you can run:

.. doctest ::

    >>> from admin import TestRunCommand, TestRunner
    >>> import unittest
    >>> suite = unittest.TestLoader().loadTestsFromTestCase(TestRunCommand)
    >>> unittest.TextTestRunner(verbosity=2).run(suite)
    <unittest.runner.TextTestResult run=1 errors=0 failures=0>
    >>> suite = unittest.TestLoader().loadTestsFromTestCase(TestRunner)
    >>> unittest.TextTestRunner(verbosity=2).run(suite)
    <unittest.runner.TextTestResult run=3 errors=0 failures=0>

You can run some of the unit tests with:

::

    python admin/__init__.py

The doctests require:

::

    cd docs
    make doctest
