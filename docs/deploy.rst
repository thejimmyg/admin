Deploy
++++++

First make sure you have an SSH host entry in ``~/.ssh/config``

Then set up a user:

::

    python -m admin.deploy $DORUN useradd --home-directory $TLS_USER_DIR $TLS_USER

Install a tarballe:

::

    python -m admin.deploy $DORUN install --directory $TLS_USER_DIR --ssh-path /root --username $TLS_USER --local-file "dobuild-tls.tar.gz"

Install a monit configuration into ``etc/monit/``:

::

    python -m admin.deploy $DORUN setup_monit_config --install-directory $TLS_USER_DIR/platform --username $TLS_USER --monit-port 8064 -password pass

Start monit:

::

    python -m admin.deploy $DORUN run_monit --install-directory $TLS_USER_DIR/platform --username $TLS_USER

Now install any monit services like SSL/TLS certificates.

Any time you change the monit configuration you have to reload monit before it will notice:

::

    python -m admin.deploy $DORUN reload_monit_config --install-directory $TLS_USER_DIR/platform --username $TLS_USER 
