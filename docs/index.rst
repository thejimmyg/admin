.. docs documentation master file, created by
   sphinx-quickstart on Thu May  5 19:23:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Admin's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 2

   admin
   vm
   test
   build
   deploy
   ssl
   monit
   django
   wsgi


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

