Monit
+++++

As part of Build and Deploy you'll have built, deployed, configured and started
monit. The Deploy docs also explain how you can reload monit so that it detects
any config changes you've made.

Each of the different sets of commands like `admin.ssl` and `admin.django` have
`monit` commands for configuring monit to monitor their services.

Once the services are configured, you'll want to be able to stop and start
services, and to report on their statuses. This is what `admin.monit` is for.
Here are some examples of the commands:

::

    python -m admin.monit $DORUN status --install-directory $TLS_USER_DIR/platform --username $TLS_USER demo_ssl 
    python -m admin.monit $DORUN stop --install-directory $TLS_USER_DIR/platform --username $TLS_USER demo_ssl 
    python -m admin.monit $DORUN status --install-directory $TLS_USER_DIR/platform --username $TLS_USER demo_ssl 
    python -m admin.monit $DORUN start --install-directory $TLS_USER_DIR/platform --username $TLS_USER demo_ssl 
    python -m admin.monit $DORUN status --install-directory $TLS_USER_DIR/platform --username $TLS_USER demo_ssl 
    sleep 5
    python -m admin.monit $DORUN status --install-directory $TLS_USER_DIR/platform --username $TLS_USER demo_ssl 

Monit doesn't automatically start services, so you must call the `start`
command at least once to get it started.
