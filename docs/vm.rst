VM
++

Create a VM named `build2` using Vagrant and Ubuntu 14.04:

.. doctest ::

    >>> from admin import cmd
    >>> import os
    >>> if not os.path.exists('_test_vms/build2/Vagrantfile'):
    ...     cmd('vm local create --img-dir _test_vms build2')
    >>> if not cmd('vm local running --img-dir _test_vms build2') == 'True':
    ...     cmd('vm local start --img-dir _test_vms build2')
    ...

Get the SSH config you need to connect to this machine:

.. doctest ::

    >>> print('Result:'); cmd('vm local ssh --img-dir _test_vms build2', out=print)
    Result:
    Host default
      HostName 127.0.0.1
      User vagrant
      Port 2222
      UserKnownHostsFile /dev/null
      StrictHostKeyChecking no
      PasswordAuthentication no
      IdentityFile ".../_test_vms/build2/.vagrant/machines/default/virtualbox/private_key"
      IdentitiesOnly yes
      LogLevel FATAL
    <BLANKLINE>

Now copy the SSH config into ``~/.ssh/config`` changing the ``Host default``
line to whatever you want (probably sensible to call it the same as the VM, in
this case ``build2``).

You can specify the IP with ``--private-ip``.  By default, Admin will give each
box an SSH port starting ``211`` and then the last part of the private IP of
the machine. For example ``21113``. You can override this default with
``--ssh-port``.

