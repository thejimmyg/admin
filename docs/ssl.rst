SSL
+++

Once you have build monit and hitch and set up its config as described in
Build, you are ready to install individual services.

The ``admin.ssl`` service helps you to configure hitch and monit.

First generate a self-signed certificate (you can skip this if you already have
a real certificate):

::

    python -m admin.ssl    $DORUN selfsignedcertificate --install-directory $TLS_USER_DIR/platform --username $TLS_USER --pem-filename jimmyg.org.pem --cn jimmyg.org

Then add a monit config for hitch:

::

    python -m admin.ssl    $DORUN monit --install-directory $TLS_USER_DIR/platform --username $TLS_USER --name demo_ssl --port 8443 --host $DORUN_IP --pem-filename jimmyg.org.pem --backend-port 8080

Now tell monit abou the new configuration:

::

    python -m admin.deploy $DORUN reload_monit_config --install-directory $TLS_USER_DIR/platform --username $TLS_USER

Finally, start the service:

::

    python -m admin.monit  $DORUN start --install-directory $TLS_USER_DIR/platform --username $TLS_USER demo_ssl

Any requests (HTTPS, WSS etc) will be forwarded from 8443 -> 8080 after being
decoded to plain services.

If you want to know the status, you can use:

::

    python -m admin.monit  $DORUN status --install-directory $TLS_USER_DIR/platform --username $TLS_USER demo_ssl

To stop the service you can run:

::

    python -m admin.monit  $DORUN start --install-directory $TLS_USER_DIR/platform --username $TLS_USER demo_ssl

Using the above configuration, hitch logs its output to syslog, so you can see
it with:

::

    tail -f /var/log/syslog

If monit has problems running hitch, you should be able to find out by looking
at the monit log:

::

    tail -f $TLS_USER_DIR/platform/log/monit.log

If you kill hitch, monit should notice within 20 seconds and start it again.

Strategy
========

Usually it makes sense to have a completely separate user and platform
for the TLS termination (since https:// and wss:// addresses always use port
443) and have the termination software proxy to different internal ports
based on the certificates.

Test server
===========

When setting up HTTPS, it is helpful to have an HTTP server to proxy to so that
you can check everything is working.

Here's such a server:

::

    from wsgiref.util import setup_testing_defaults
    from wsgiref.simple_server import make_server

    # A relatively simple WSGI application. It's going to print out the
    # environment dictionary after being updated by setup_testing_defaults
    def simple_app(environ, start_response):
        setup_testing_defaults(environ)

        status = '200 OK'
        headers = [('Content-type', 'text/plain; charset=utf-8')]

        start_response(status, headers)

        ret = [("%s: %s\n" % (key, value)).encode("utf-8")
               for key, value in environ.items()]
        return ret

    httpd = make_server('', 8000, simple_app)
    print("Serving on port 8000...")
    httpd.serve_forever()
