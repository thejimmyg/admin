Django
++++++

Create your own build addon ``jimmyg_build.py``:

.. literalinclude:: ../examples/jimmyg/jimmyg_build.py
   :language: python

You can then run:

::

    python -m jimmyg_build $DOBUILD pipdeps --install-directory $JIMMYG_USER_DIR/platform $JIMMYG_USER

This will install all the pip dependencies. They will then get built into the tarball and deployed when you deploy via the normal processes.

Once you have it all running you need:

::

    /opt/jimmyg/platform/bin/pyvenv-3.5 --system-site-packages .ve
    export PATH=/opt/jimmyg/platform/bin:/opt/jimmyg/platform/sbin:$PATH

Then to upload Django:

::

    rsync -aHxv examples/jimmyg/app jimmyg@dorun:app 

Once there, here's how you run everything:

::
    
    daphne -b 46.101.92.6 -p 8080 jimmyg.asgi:channel_layer
    bin/redis-server
    python manage.py runworker
    
You can test with:

::

    curl http://46.101.92.6:8080

Redis problems that need addessing:

::

    5703:C 04 Aug 07:30:00.020 # Warning: no config file specified, using the default config. In order to specify a config file use bin/redis-server /path/to/redis.conf
    5703:M 04 Aug 07:30:00.021 # You requested maxclients of 10000 requiring at least 10032 max file descriptors.
    5703:M 04 Aug 07:30:00.021 # Server can't set maximum open files to 10032 because of OS error: Operation not permitted.
    5703:M 04 Aug 07:30:00.021 # Current maximum open files is 4096. maxclients has been reduced to 4064 to compensate for low ulimit. If you need higher maxclients increase 'ulimit -n'.
                    _._                                                  
               _.-``__ ''-._                                             
          _.-``    `.  `_.  ''-._           Redis 3.2.0 (00000000/0) 64 bit
      .-`` .-```.  ```\/    _.,_ ''-._                                   
     (    '      ,       .-`  | `,    )     Running in standalone mode
     |`-._`-...-` __...-.``-._|'` _.-'|     Port: 6379
     |    `-._   `._    /     _.-'    |     PID: 5703
      `-._    `-._  `-./  _.-'    _.-'                                   
     |`-._`-._    `-.__.-'    _.-'_.-'|                                  
     |    `-._`-._        _.-'_.-'    |           http://redis.io        
      `-._    `-._`-.__.-'_.-'    _.-'                                   
     |`-._`-._    `-.__.-'    _.-'_.-'|                                  
     |    `-._`-._        _.-'_.-'    |                                  
      `-._    `-._`-.__.-'_.-'    _.-'                                   
          `-._    `-.__.-'    _.-'                                       
              `-._        _.-'                                           
                  `-.__.-'                                               
    
    5703:M 04 Aug 07:30:00.023 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
    5703:M 04 Aug 07:30:00.023 # Server started, Redis version 3.2.0
    5703:M 04 Aug 07:30:00.023 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
    5703:M 04 Aug 07:30:00.023 * The server is now ready to accept connections on port 6379
    5703:M 04 Aug 07:35:32.098 * 100 changes in 300 seconds. Saving...
    5703:M 04 Aug 07:35:32.099 * Background saving started by pid 5951
    5951:C 04 Aug 07:35:32.102 * DB saved on disk


Also:

* PostgreSQL make test?  No, make check.
